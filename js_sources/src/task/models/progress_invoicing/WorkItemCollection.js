import BaseCollection from "base/models/BaseCollection"
import WorkItemModel from "./WorkItemModel"

const WorkItemCollection = BaseCollection.extend({
    model: WorkItemModel,
})
export default WorkItemCollection
