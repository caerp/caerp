import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import { ajax_call } from "../../tools.js"

const ConfigBusClass = Mn.Object.extend({
    channelName: "config",
    radioRequests: {
        "get:options": "getFormOptions",
        "has:section": "hasFormSection",
        "get:section": "getFormSection",
        "get:actions": "getFormActions",
        "has:form_section": "hasFormSection",
        "get:form_section": "getFormSection",
        "get:form_actions": "getFormActions",
        reload_config: "start",
    },
    setFormConfig(form_config) {
        console.log(" -> ConfigBus.started : form_config loaded")
        console.log(form_config)
        this.form_config = form_config
    },
    getFormOptions(option_name) {
        /*
         * Return a clone of the form options for option_name
         *
         * :param str option_name: The name of the option
         * :returns: A list of dict with options (for building selects)
         */
        let options = this.form_config["options"][option_name]
        if (_.isArray(options)) {
            return _.map(options, _.clone)
        } else {
            return _.clone(options)
        }
    },
    hasFormSection(section_name) {
        /*
         *
         * :param str section_name: The name of the section can be a path of attributes separated by ':'
         * :rtype: bool
         */
        let section = this.getFormSection(section_name)
        return section !== null
    },
    getFormSection(section_name) {
        /*
         *
         * Return the form section description
         * :param str section_name: The name of the section can be a path of attributes separated by ':'
         * :returns: The section definition
         * :rtype: Object
         */
        const sections = section_name.split(":")
        let result = this.form_config["sections"]
        for (let section_name of sections) {
            if (section_name in result) {
                result = result[section_name]
            } else {
                result = null
            }
        }
        return result
    },
    getFormActions() {
        /*
         * Return available form action config
         */
        return this.form_config["actions"]
    },
    _getFieldSettings(modelName, fieldName) {
        let modelsSettings = this.form_config.models_settings || {}
        let modelSettings = modelsSettings[modelName] || {}
        let fieldSettings = modelSettings[fieldName] || {}
        return fieldSettings
    },
    isFieldMasked(modelName, fieldName) {
        let fieldSettings = this._getFieldSettings(modelName, fieldName)
        return fieldSettings.masked || false
    },
    isFieldRequired(modelName, fieldName) {
        let fieldSettings = this._getFieldSettings(modelName, fieldName)
        return fieldSettings.required || false
    },
    setup(url) {
        /*
         * Set the configuration url
         */
        this._url = url
    },
    start() {
        /*
         * Load the page configuration at the given url
         */
        let request = ajax_call(this._url)
        request = request.then(this.setFormConfig.bind(this), (message) =>
            console.log(message)
        )
        return request
    },
})
const ConfigBus = new ConfigBusClass()
export default ConfigBus
