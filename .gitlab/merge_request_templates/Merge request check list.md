## Changements apportés

<!-- Décrire les changements qu'apporte cette MR-- >


## Checklist

<!-- À valider avant de demander une review -->

- Style UX
  * [ ] Suit le [guide de style](https://framagit.org/endi/endi/-/blob/master/docs/source/styleguide.rst) écrit par Sophie.
- Review
  * [ ] La MR est assignée à une personne pertinente pour en faire une review
- Git
  * [ ] Rebasé sur master
  * [ ] Historique des commits nettoyé (rebase interactif)
  * [ ] Messages de commit mentionnant l'issue (Ref #XXX : ou Fix #XXX :)
- Tests
  * [ ] Tests "verts" en local (pytest)
  * [ ] Validé par la CI
- Code propre
  * [ ] [PEP8](https://www.python.org/dev/peps/pep-0008/) flake8
  * [ ] Conventions de nommage des variables, des fonctions et des classes respectées
  * [ ] Typo dans les variables, des fonctions et des classes
  * [ ] Typo dans les chaînes de caractères
  * [ ] Code commenté
  * [ ] Commentaires morts (inutile, tests) nettoyés
- Base de données
  * [ ] Appliqué toutes les migrations présentes amont
  * [ ] [Pas d'import de modèles dans les migrations](https://stackoverflow.com/a/45805092)
  * [ ] Créer une migration de merge si nécessaire
  * [ ] [endi_anonymize.py](https://framagit.org/endi/endi/blob/master/endi/scripts/endi_anonymize.py)
    mis à jour si besoin (ajout/modif de modèle).
  * [ ] j'évite d'utiliser `backref=` au profit de `back_populates=` dans les `relationship`

## Points de vigilance / Retours souhaités

<!-- une partie du code sur laquelle on a un doute (qualité, logique fonctionnelle) -->

## Navigateurs testés

<!-- Remplir la liste avec le/les navigateurs qui ont servi aux tests et leur version -->


