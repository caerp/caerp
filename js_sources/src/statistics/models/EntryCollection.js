import Bb from "backbone"
import EntryModel from "./EntryModel.js"

const EntryCollection = Bb.Collection.extend({
    model: EntryModel,
})
export default EntryCollection
