import Mn from "backbone.marionette"

import ActionToolbar from "../views/ActionToolbar"

const ToolbarAppClass = Mn.Application.extend({
    /*
  Base classe for Toolbar management
  */
    region: "#js_actions",
    getResumeView(actions) {
        return null // default
    },
    getMainActions(actions) {
        return actions["main"]
    },
    getMoreActions(actions) {
        return actions["more"]
    },
    renderApp(actions) {
        const main_view = this.getMainActions(actions)
        const more_view = this.getMoreActions(actions)
        const resume_view = this.getResumeView(actions)

        if (
            (main_view != null && main_view.length > 0) ||
            (more_view != null && more_view.length > 0) ||
            (resume_view != null && resume_view.length > 0)
        ) {
            this.view = new ActionToolbar({
                main: main_view,
                more: more_view,
                resume: resume_view,
            })
            this.showView(this.view)
        }
    },
    onStart(app, actions) {
        this.renderApp(actions)
    },
})
export default ToolbarAppClass
