Export des écritures au format Sage Generation expert
=======================================================

CAERP permet de configurer l'export des écritures au format compatible avec ce
qui est attendu par Sage Generation Expert.

Afin de configurer CAERP pour utiliser ces modules, la configuration suivante
doit être ajoutée dans la section "[app:caerp]" du fichier .ini

.. code-block::

    caerp.services.treasury_invoice_producer=caerp.compute.sage_generation_expert.compute.InvoiceProducer
    caerp.services.treasury_internalinvoice_producer=caerp.compute.sage_generation_expert.compute.InternalInvoiceProducer
    caerp.services.treasury_invoice_writer=caerp.export.sage_generation_expert.InvoiceWriter

    caerp.services.treasury_payment_producer=caerp.compute.sage_generation_expert.compute.PaymentProducer
    caerp.services.treasury_internalpayment_producer=caerp.compute.sage_generation_expert.compute.InternalPaymentProducer
    caerp.services.treasury_payment_writer=caerp.export.sage_generation_expert.PaymentWriter

    caerp.services.treasury_expense_producer=caerp.compute.sage_generation_expert.compute.ExpenseProducer
    caerp.services.treasury_expense_writer=caerp.export.sage_generation_expert.ExpenseWriter

    caerp.services.treasury_supplier_invoice_producer=caerp.compute.sage_generation_expert.compute.SupplierInvoiceProducer
    caerp.services.treasury_internalsupplier_invoice_producer=caerp.compute.sage_generation_expert.compute.InternalSupplierInvoiceProducer
    caerp.services.treasury_supplier_invoice_writer=caerp.export.sage_generation_expert.SupplierInvoiceWriter

    caerp.services.treasury_supplier_payment_producer=caerp.compute.sage_generation_expert.compute.SupplierPaymentProducer
    caerp.services.treasury_supplier_payment_user_producer=caerp.compute.sage_generation_expert.compute.SupplierUserPaymentProducer
    caerp.services.treasury_internalsupplier_payment_producer=caerp.compute.sage_generation_expert.compute.InternalSupplierPaymentProducer
    caerp.services.treasury_supplier_payment_writer=caerp.export.sage_generation_expert.SupplierPaymentWriter

    caerp.services.treasury_expense_payment_producer=caerp.compute.sage_generation_expert.compute.ExpensePaymentProducer
    caerp.services.treasury_expense_payment_writer=caerp.export.sage_generation_expert.ExpensePaymentWriter
