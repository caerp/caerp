import Bb from "backbone"
import PostTTCLineModel from "./PostTTCLineModel.js"
import Radio from "backbone.radio"

const PostTTCLineCollection = Bb.Collection.extend({
    model: PostTTCLineModel,
    initialize: function (options) {
        this.on("saved", this.channelCall)
        this.on("destroyed", this.channelCall)
    },
    channelCall: function () {
        var channel = Radio.channel("facade")
        channel.trigger("changed:post_ttc_line")
    },
    validate: function () {
        var result = {}
        this.each(function (model) {
            var res = model.validate()
            if (res) {
                _.extend(result, res)
            }
        })
        return result
    },
})
export default PostTTCLineCollection
