import Bb from "backbone"
import RelatedEstimationModel from "./RelatedEstimationModel"

const RelatedEstimationCollection = Bb.Collection.extend({
    model: RelatedEstimationModel,
})
export default RelatedEstimationCollection
