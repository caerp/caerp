import Radio from "backbone.radio"
import Mn from "backbone.marionette"

const Controller = Mn.Object.extend({
    initialize(options) {
        this.facade = Radio.channel("facade")
        this.rootView = options["rootView"]
    },
    index: function () {
        this.rootView.render()
    },
})
export default Controller
