import pytest
from sqlalchemy import func
from caerp.controllers.expense_types import ExpenseTypeQueryService
from caerp.models.expense.types import ExpenseType


@pytest.fixture
def active_expense_type(mk_expense_type):
    return mk_expense_type("ET1", 1)


@pytest.fixture
def inactive_expense_type(mk_expense_type):
    return mk_expense_type("ET2", 2, active=False)


@pytest.fixture
def active_expensekm_type(mk_expense_type):
    return mk_expense_type(
        label="Déplacement cat1",
        code="cat1",
        amount=10,
    )


@pytest.fixture
def active_expensekm_type_issue_2098(mk_expense_type):
    return mk_expense_type(
        label="Déplacement - cat1",
        code="cat1",
        amount=10,
    )


@pytest.fixture
def inactive_expensekm_type(mk_expense_type):
    return mk_expense_type(
        label="Déplacement cat2",
        code="cat2",
        amount=10,
        active=False,
    )


@pytest.fixture
def unrestricted_user(user):
    return user


@pytest.fixture
def restricted_user_cat2(mk_user):
    # Restricted to cat2, which is disabled
    return mk_user(
        lastname="Restricted Lastname",
        firstname="Restricted Firstname",
        email="restricted@c.fr",
        vehicle="Déplacement cat2-cat2",
    )


@pytest.fixture
def restricted_user_cat1(mk_user):
    # Restricted to cat2, which is disabled
    return mk_user(
        lastname="Restricted Lastname",
        firstname="Restricted Firstname",
        email="restricted@c.fr",
        vehicle="Déplacement cat1-cat1",
    )


@pytest.fixture
def restricted_user_cat1_issue_2098(mk_user):
    # Restricted to cat2, which is disabled
    return mk_user(
        lastname="Restricted Lastname",
        firstname="Restricted Firstname",
        email="restricted@c.fr",
        vehicle="Déplacement - cat1-cat1",
    )


class TestExpenseTypeQueryService:
    def test__type_ids_from_line(
        self,
        mk_expense_kmline,
        mk_expense_line,
        mk_expense_type,
    ):
        lines = []
        km_lines = []
        for i in range(5):
            km = mk_expense_type(amount=10)
            typ = mk_expense_type()
            lines.append(mk_expense_line(type_id=typ.id))
            km_lines.append(mk_expense_kmline(type_id=km.id))
        res = ExpenseTypeQueryService._type_ids_from_lines(lines, km_lines)
        assert len(res) == 10

    def test_purchase_and_expense_options(
        self, dbsession, mk_expense_type, mk_expense_line
    ):
        for i in range(5):
            mk_expense_type(category=None)
            mk_expense_type(category=ExpenseType.PURCHASE_CATEGORY)
            mk_expense_type(category=ExpenseType.PURCHASE_CATEGORY, active=False)
            mk_expense_type(category=ExpenseType.EXPENSE_CATEGORY)
            mk_expense_type(category=ExpenseType.EXPENSE_CATEGORY, active=False)
        # Tel expense : doit être renvoyé dans les "expense_options"
        mk_expense_type(percentage=50)

        for i in range(3):
            mk_expense_type(category=ExpenseType.PURCHASE_CATEGORY, internal=True)

        typ = mk_expense_type(category=ExpenseType.PURCHASE_CATEGORY, active=False)
        exp = mk_expense_line(type_id=typ.id)
        res = (
            dbsession.execute(ExpenseTypeQueryService.purchase_options())
            .scalars()
            .all()
        )
        # Il existe un type par défaut dans le conftest
        assert len(res) == 11
        res = (
            dbsession.execute(ExpenseTypeQueryService.purchase_options(False, [exp]))
            .scalars()
            .all()
        )
        assert len(res) == 12
        res = (
            dbsession.execute(ExpenseTypeQueryService.purchase_options(True))
            .scalars()
            .all()
        )
        assert len(res) == 3

        typ = mk_expense_type(ExpenseType.EXPENSE_CATEGORY, active=False)
        exp = mk_expense_line(type_id=typ.id)
        assert (
            len(
                dbsession.execute(ExpenseTypeQueryService.expense_options())
                .scalars()
                .all()
            )
            == 12
        )
        assert (
            len(
                dbsession.execute(ExpenseTypeQueryService.expense_options([exp]))
                .scalars()
                .all()
            )
            == 13
        )

    def test__is_user_vehicle_query(
        self,
        dbsession,
        active_expensekm_type,
        active_expensekm_type_issue_2098,
        restricted_user_cat1,
        restricted_user_cat1_issue_2098,
        restricted_user_cat2,
        unrestricted_user,
    ):
        query = ExpenseTypeQueryService._is_user_vehicle_query(
            restricted_user_cat1, 2018
        )
        assert dbsession.execute(query).scalars().all() == [active_expensekm_type.id]
        query = ExpenseTypeQueryService._is_user_vehicle_query(
            restricted_user_cat2, 2018
        )
        assert dbsession.execute(query).scalars().all() == []
        query = ExpenseTypeQueryService._is_user_vehicle_query(
            restricted_user_cat1_issue_2098, 2018
        )
        assert dbsession.execute(query).scalars().all() == [
            active_expensekm_type_issue_2098.id
        ]
        query = ExpenseTypeQueryService._is_user_vehicle_query(unrestricted_user, 2018)
        assert dbsession.execute(query).scalars().all() == [
            active_expensekm_type.id,
            active_expensekm_type_issue_2098.id,
        ]

    def test_expensekm_options(
        self,
        dbsession,
        active_expensekm_type,
        active_expensekm_type_issue_2098,
        restricted_user_cat1,
        inactive_expensekm_type,
        mk_expense_kmline,
    ):
        lines = [mk_expense_kmline(type_id=inactive_expensekm_type.id)]
        query = ExpenseTypeQueryService.expensekm_options(
            restricted_user_cat1, 2018, lines
        )
        assert dbsession.execute(query).scalars().all() == [
            active_expensekm_type,
            inactive_expensekm_type,
        ]


# def test_allowed_driver(
#     unrestricted_user,
#     restricted_user_cat1,
#     restricted_user_cat1_issue_2098,
#     restricted_user_cat2,
#     active_expensekm_type,
#     active_expensekm_type_issue_2098,
#     inactive_expense_type,
# ):
#     assert ExpenseTypeQueryService.allowed_driver(unrestricted_user, 2018).all() == [
#         active_expensekm_type,
#         active_expensekm_type_issue_2098,
#     ]

#     # It has only the disabled expensetype allowed, thus, nothing available
#     assert ExpenseTypeQueryService.allowed_driver(restricted_user_cat2, 2018).all() == []

#     assert ExpenseTypeQueryService.allowed_driver(restricted_user_cat1, 2018).all() == [
#         active_expensekm_type
#     ]
#     assert ExpenseTypeQueryService.allowed_driver(
#         restricted_user_cat1_issue_2098, 2018
#     ).all() == [active_expensekm_type_issue_2098]


# def test_expensekm_options():
#     pass
