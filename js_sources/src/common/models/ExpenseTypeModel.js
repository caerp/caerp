import Bb from "backbone"

import { getPercent } from "../../math.js"

const ExpenseTypeModel = Bb.Model.extend({
    props: [
        "code",
        "value",
        "label",
        "active",
        // For tel only
        "percentage",
        "id",
        // For km only
        "amount",
        // Read-only
        "family",
    ],
    defaults: {
        // Generalize special tel computation
        percentage: 100,
    },
    computeAmount(amount) {
        /**
         * Compute the amount of the expense to be handled by CAE.
         */
        let result
        if (this.has("percentage") && this.get("percentage") != 100) {
            result = getPercent(amount, this.get("percentage"))
        } else {
            // We want to avoid rounding if 100%.
            result = amount
        }
        return result
    },
})
export default ExpenseTypeModel
