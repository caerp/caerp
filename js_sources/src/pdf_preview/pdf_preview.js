/*
Usage :

    Vue Python : 
        from caerp.resources import pdf_preview_js
        pdf_preview_js.need()

    Template, script JS : 
        $(() => pdf_preview.render('https://example.com/document.pdf', showControls=false, region='#mytag'));

*/
import Mn from "backbone.marionette"
import PdfViewerView from "common/views/PDFViewerView.js"

const AppClass = Mn.Application.extend({
    onBeforeStart(app, options) {
        this.rootView = new PdfViewerView({
            fileUrl: options.url,
            showControls: options.showControls,
        })
    },
    onStart(app, options) {
        this.showView(this.rootView)
    },
})

export function render(url, showControls = true, region = ".pdfpreview") {
    const app = new AppClass({ region: region })
    app.start({ url: url, showControls: showControls })
}
