Configuration de mysql/mariadb
===============================

 .. note:: S'agissant d'une instance de développement utilisant docker-compose,
           l'enchainement de commandes suivantes devrait faire l'affaire et
           remplacer le déroulé de la doc (attention, efface la BDD) :

          .. code-block::

             make dev_db_clear
             make dbv_db_start
             caerp-load-demo-data development.ini
             caerp-migrate development.ini upgrade
             docker-compose -f db-docker-compose.yaml run --rm mariadb  \
                 mysqlcheck -u root -proot -P 3306 -hmariadb --auto-repair --optimize --all-databases
             MYSQL_CMD="docker-compose -f db-docker-compose.yaml run --rm mariadb mysql -u root -proot -P 3306 -hmariadb" ./tools/migrate_encoding.sh


Afin de gérer correctement les différents types de caractères, nous utilisons
le format utf8mb4.

Il faut pour cela configurer correctement le serveur mariadb correspondant :

  - Sous fedora/RHEL : dans un des fichiers /etc/my.cnf ou un fichier du répertoire /etc/my.cnf.d/

  - Sous Debian/Ubuntu : dans un des fichiers /etc/mysql/mariadb.cnf ou un fichier du répertoire /etc/mysql/mariadb.conf.d/

.. code-block::

    [mysql]
    default-character-set=utf8mb4


    [mysqld]
    # Permet d'assurer que la connexion du client est forcée
    character-set-client-handshake = FALSE
    collation-server = utf8mb4_unicode_ci
    init-connect = 'SET NAMES utf8mb4 COLLATE utf8mb4_unicode_ci'
    character-set-server = utf8mb4


NB : le serveur mariadb (ou mysql) doit être relancé après modification

Si ces modifications ont été effectuées sur un serveur existant, il faut lancer
la commande suivante (le -p si votre utilisateur root mysql a un mot de passe)

.. code-block:: console

    mysqlcheck -u root -p --auto-repair --optimize --all-databases


Configuration de CAERP
===============================
Une fois la base de données configurée avec l'encodage utf8mb4 il faut indiquer à CAERP de l'utiliser.

Pour cela il faut modifier les fichiers de config (.ini) de **caerp** et **caerp\_celery** et modifier l'encodage passé en paramètre à la chaîne de connexion mysql :

.. code-block:: console

  sqlalchemy.url = mysql://caerp:caerp@127.0.0.1:13306/caerp?charset=utf8mb4


Migration de base de données existantes
=========================================

La commande suivante migre les tables et colonne de la base de données 'caerp'
vers l'encodage utf8mb4. Elle peut être lancée plusieurs fois (le -p si votre
utilisateur root mysql a un mot de passe).

.. code-block:: console

   MYSQL_CMD='mysql -u root -p' ./tools/migrate_encoding.sh
