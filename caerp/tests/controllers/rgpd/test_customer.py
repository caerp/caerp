from caerp.controllers.rgpd.customer import rgpd_clean_customer


def test_rgpd_clean_customer(csrf_request_with_db_and_user, mk_customer):
    data = dict(
        type="individual",
        firstname="John",
        lastname="Doe",
        email="john@example.com",
        mobile="0612345678",
        fax="0123456789",
        phone="062154",
        address="123 Main St",
        additional_address="Apt 4B",
        city="New York",
        zip_code="10001",
        country="US",
    )
    customer = mk_customer(**data)
    rgpd_clean_customer(csrf_request_with_db_and_user, customer)
    for key in data:
        if key == "type":
            continue
        assert getattr(customer, key) == ""
    assert len(customer.statuses) == 1
    assert customer.statuses[0].label == "[RGPD]"
