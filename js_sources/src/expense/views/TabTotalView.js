import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import { formatAmount } from "../../math.js"

const TabTotalView = Mn.View.extend({
    tagName: "div",
    template: require("./templates/TabTotalView.mustache"),
    modelEvents: {
        "change:ttc": "render",
        "change:km_ttc": "render",
    },
    templateContext() {
        var category = this.getOption("category")
        return {
            ttc: formatAmount(
                this.model.get("ttc_" + category) +
                    this.model.get("km_ttc_" + category)
            ),
        }
    },
})
export default TabTotalView
