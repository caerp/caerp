import Bb from "backbone"
import Mn from "backbone.marionette"
import Router from "./Router.js"
import { hideLoader } from "../../tools.js"
import MainView from "../views/MainView.js"
import Controller from "./Controller.js"
import ConfigBus from "../../base/components/ConfigBus.js"

import ExpenseTypeService from "../../common/components/ExpenseTypeService.js"

const AppClass = Mn.Application.extend({
    region: "#js-main-area",
    channelName: "app",
    onBeforeStart(app, options) {
        console.log("AppClass.onBeforeStart")
        this.rootView = new MainView()
        this.controller = new Controller({ rootView: this.rootView })
        this.router = new Router({ controller: this.controller })
        console.log("AppClass.onBeforeStart finished")
        ExpenseTypeService.setFormConfig(ConfigBus.form_config)
    },
    onStart(app, options) {
        this.showView(this.rootView)
        console.log("Starting the history")
        hideLoader()
        Bb.history.start()
    },
})
const App = new AppClass()
export default App
