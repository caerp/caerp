from caerp.views.company.rest_api import CompanyRestView


def test_add(config, call_rest_view):
    json_response = call_rest_view(
        CompanyRestView,
        method="post",
        post=dict(name="Fu"),
    )
    assert json_response["name"] == "Fu"
    assert json_response.get("id") is not None
