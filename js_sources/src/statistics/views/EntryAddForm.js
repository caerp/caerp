import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import FormBehavior from "../../base/behaviors/FormBehavior"
import InputWidget from "../../widgets/InputWidget"
import TextAreaWidget from "../../widgets/TextAreaWidget"

const template = require("./templates/EntryAddForm.mustache")

const EntryAddForm = Mn.View.extend({
    template: template,
    className: "main_content",
    behaviors: [FormBehavior],
    regions: {
        title: ".field-title",
        description: ".field-description",
    },
    initialize() {
        this.config = Radio.channel("config")
    },
    onSuccessSync() {
        const app = Radio.channel("app")
        app.trigger("navigate", "entries/" + this.model.get("id"))
    },
    onCancelForm() {
        let app = Radio.channel("app")
        if (this.getOption("add")) {
            app.trigger("navigate", "index")
        } else {
            app.trigger("navigate", "entries/" + this.model.get("id"))
        }
    },
    templateContext() {
        // Collect data sent to the template (model attributes are already transmitted)
        let title
        if (this.getOption("add")) {
            title = "Ajouter une entrée statistique"
        } else {
            title = "Modifier une entrée statistique"
        }
        return { title: title }
    },
    onRender() {
        console.log(this.model)
        this.showChildView(
            "title",
            new InputWidget({
                field_name: "title",
                label: "Intitulé de l'entrée statistique",
                description:
                    "Intitulé de l'entrée statistique tel qu'il figurera dans l'export",
                required: true,
                value: this.model.get("title"),
            })
        )
        this.showChildView(
            "description",
            new TextAreaWidget({
                field_name: "description",
                label: "Description de l'entrée statistique",
                description:
                    "Description de l'entrée statistique tel qu'elle figurera dans l'export",
                value: this.model.get("description"),
                required: false,
            })
        )
    },
})
export default EntryAddForm
