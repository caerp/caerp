import pytest
import datetime
from caerp.utils.security.acls import (
    get_estimation_default_acl,
    get_invoice_default_acl,
)
from caerp.tests.tools import (
    Dummy,
    check_acl,
)
from caerp.models.task import (
    TaskLineGroup,
)


@pytest.fixture
def company(mk_company):
    return mk_company(id=1)


@pytest.fixture
def dummy_project():
    project_type = Dummy(default=True, with_business=False)
    project = Dummy(project_type=project_type, company_id=1)
    return project


@pytest.fixture
def estimation(mk_estimation, mk_project, mk_project_type, company):
    project_type = mk_project_type(name="default", with_business=False)
    project = mk_project(project_type=project_type, company=company)
    return mk_estimation(
        status="draft",
        company=company,
        signed_status="waiting",
        geninv=False,
        type_="estimation",
        project=project,
        internal=False,
    )


@pytest.fixture
def invoice(
    dbsession,
    mk_invoice,
    mk_project,
    mk_project_type,
    company,
    task_line,
    task_line_group,
):
    inv = mk_invoice(
        status="draft",
        paid_status="waiting",
        exported=False,
        invoicing_mode="classic",
        internal=False,
        supplier_invoice_id=None,
        company=company,
    )
    inv.line_groups = [task_line_group]
    inv = dbsession.merge(inv)
    dbsession.flush()
    return inv


@pytest.fixture
def estimation_exceed_limit_amount(dbsession, mk_task_line, estimation):
    line = mk_task_line(
        tva=200, cost=20000000, description="Product line"  # 200 euros HT
    )
    estimation.line_groups = [TaskLineGroup(lines=[line])]
    dbsession.merge(estimation)
    dbsession.flush()
    return estimation


@pytest.fixture
def invoice_exceed_limit_amount(dbsession, mk_task_line, invoice):
    line = mk_task_line(
        tva=200, cost=20000000, description="Product line"  # 200 euros HT
    )
    invoice.line_groups = [TaskLineGroup(lines=[line])]
    dbsession.merge(invoice)
    dbsession.flush()
    return invoice


@pytest.fixture
def internalinvoice(mk_internalinvoice, mk_project, mk_project_type, company):
    return mk_internalinvoice(
        status="draft",
        company=company,
        paid_status="waiting",
        exported=False,
        invoicing_mode="classic",
        CLASSIC_MODE="classic",
        internal=True,
        supplier_invoice_id=None,
    )


@pytest.fixture
def cancelinvoice():
    return Dummy(
        status="draft",
        company_id=1,
        exported=False,
        type_="cancelinvoice",
        internal=False,
    )


@pytest.fixture
def expense_sheet():
    return Dummy(
        status="draft",
        company_id=1,
        paid_status="waiting",
        expense_exported=False,
        purchase_exported=False,
        type_="expensesheet",
    )


@pytest.fixture
def supplier_order_exceed_limit_amount(
    dbsession,
    mk_supplier_order_line,
    company,
    supplier_order,
):
    line = mk_supplier_order_line(
        description="Commande fournisseur",
        ht=10000000,
        tva=100,
        supplier_order=supplier_order,
    )
    supplier_order.lines = [line]
    dbsession.merge(supplier_order)
    dbsession.flush()
    return supplier_order


@pytest.fixture
def supplier_invoice_exceed_limit_amount(
    dbsession,
    mk_supplier_invoice_line,
    company,
    supplier_invoice,
):
    line = mk_supplier_invoice_line(
        description="Facture fournisseur",
        ht=10000000,
        tva=100,
        supplier_invoice=supplier_invoice,
    )
    supplier_invoice.lines = [line]
    dbsession.merge(supplier_invoice)
    dbsession.flush()
    return supplier_invoice


@pytest.fixture
def acl_request(get_csrf_request, user):
    return get_csrf_request(user=user)


def test_supplier_order(
    supplier_order, supplier_order_exceed_limit_amount, acl_request, user
):
    from caerp.utils.security.acls import get_supplier_order_default_acl

    # Draft acls
    acl = get_supplier_order_default_acl(supplier_order)
    assert not check_acl(acl, "context.validate_supplier_order", "company:1")
    assert check_acl(
        acl,
        "context.validate_supplier_order",
        "access_right:es_validate_supplier_order",
    )

    # Wait acls
    supplier_order.status = "wait"
    acl = get_supplier_order_default_acl(supplier_order)
    assert not check_acl(acl, "context.validate_supplier_order", "company:1")
    assert check_acl(
        acl,
        "context.validate_supplier_order",
        "access_right:es_validate_supplier_order",
    )

    # Invalid acls
    supplier_order.status = "invalid"
    acl = get_supplier_order_default_acl(supplier_order)
    assert not check_acl(acl, "context.validate_supplier_order", "company:1")
    assert check_acl(
        acl,
        "context.validate_supplier_order",
        "access_right:es_validate_supplier_order",
    )

    # Valid acls
    supplier_order.status = "valid"
    acl = get_supplier_order_default_acl(supplier_order)
    assert not check_acl(acl, "context.validate_supplier_order", "company:1")
    assert not check_acl(
        acl,
        "context.validate_supplier_order",
        "access_right:es_validate_supplier_order",
    )


def test_supplier_order_exceed_limit_amount(
    supplier_order_exceed_limit_amount, acl_request, login, user
):
    from caerp.utils.security.acls import get_supplier_order_default_acl

    login.supplier_order_limit_amount = 100

    # Draft acls and invoice amount > supplier_order_limit_amount

    acl = get_supplier_order_default_acl(supplier_order_exceed_limit_amount)
    assert not check_acl(acl, "context.validate_supplier_order", "company:1")
    assert not check_acl(
        acl,
        "context.validate_supplier_order",
        "access_right:es_validate_supplier_order",
    )

    # Wait acls  and invoice amount > supplier_order_limit_amount
    supplier_order_exceed_limit_amount.status = "wait"
    acl = get_supplier_order_default_acl(supplier_order_exceed_limit_amount)
    assert not check_acl(acl, "context.validate_supplier_order", "company:1")
    assert not check_acl(
        acl,
        "context.validate_supplier_order",
        "access_right:es_validate_supplier_order",
    )

    # Invalid acls and invoice amount > supplier_order_limit_amount
    supplier_order_exceed_limit_amount.status = "invalid"
    acl = get_supplier_order_default_acl(supplier_order_exceed_limit_amount)
    assert not check_acl(acl, "context.validate_supplier_order", "company:1")
    assert not check_acl(
        acl,
        "context.validate_supplier_order",
        "access_right:es_validate_supplier_order",
    )

    # Valid acls and invoice amount > supplier_order_limit_amount
    supplier_order_exceed_limit_amount.status = "valid"
    acl = get_supplier_order_default_acl(supplier_order_exceed_limit_amount)
    assert not check_acl(acl, "context.validate_supplier_order", "company:1")
    assert not check_acl(
        acl,
        "context.validate_supplier_order",
        "access_right:es_validate_supplier_order",
    )


def test_supplier_invoice(supplier_invoice, acl_request, user):
    from caerp.utils.security.acls import get_supplier_invoice_acl

    # Draft acls
    acl = get_supplier_invoice_acl(supplier_invoice)
    assert not check_acl(acl, "context.validate_supplier_invoice", "company:1")
    assert check_acl(
        acl,
        "context.validate_supplier_invoice",
        "access_right:es_validate_supplier_invoice",
    )

    # Wait acls
    supplier_invoice.status = "wait"
    acl = get_supplier_invoice_acl(supplier_invoice)
    assert not check_acl(acl, "context.validate_supplier_invoice", "company:1")
    assert check_acl(
        acl,
        "context.validate_supplier_invoice",
        "access_right:es_validate_supplier_invoice",
    )

    # Invalid acls
    supplier_invoice.status = "invalid"
    acl = get_supplier_invoice_acl(supplier_invoice)
    assert not check_acl(acl, "context.validate_supplier_invoice", "company:1")
    assert check_acl(
        acl,
        "context.validate_supplier_invoice",
        "access_right:es_validate_supplier_invoice",
    )

    # Valid acls
    supplier_invoice.status = "valid"
    acl = get_supplier_invoice_acl(supplier_invoice)
    assert not check_acl(acl, "context.validate_supplier_invoice", "company:1")
    assert not check_acl(
        acl,
        "context.validate_supplier_invoice",
        "access_right:es_validate_supplier_invoice",
    )


def test_supplier_invoice_exceed_limit_amount(
    supplier_invoice_exceed_limit_amount, acl_request, user, login
):
    from caerp.utils.security.acls import get_supplier_invoice_acl

    login.supplier_order_limit_amount = 100

    # Draft acls
    acl = get_supplier_invoice_acl(supplier_invoice_exceed_limit_amount)
    assert not check_acl(acl, "context.validate_supplier_invoice", "company:1")
    assert check_acl(
        acl,
        "context.validate_supplier_invoice",
        "access_right:es_validate_supplier_invoice",
    )

    # Wait acls
    supplier_invoice_exceed_limit_amount.status = "wait"
    acl = get_supplier_invoice_acl(supplier_invoice_exceed_limit_amount)
    assert not check_acl(acl, "context.validate_supplier_invoice", "company:1")
    assert check_acl(
        acl,
        "context.validate_supplier_invoice",
        "access_right:es_validate_supplier_invoice",
    )

    # Invalid acls
    supplier_invoice_exceed_limit_amount.status = "invalid"
    acl = get_supplier_invoice_acl(supplier_invoice_exceed_limit_amount)
    assert not check_acl(acl, "context.validate_supplier_invoice", "company:1")
    assert check_acl(
        acl,
        "context.validate_supplier_invoice",
        "access_right:es_validate_supplier_invoice",
    )

    # Valid acls
    supplier_invoice_exceed_limit_amount.status = "valid"
    acl = get_supplier_invoice_acl(supplier_invoice_exceed_limit_amount)
    assert not check_acl(acl, "context.validate_supplier_invoice", "company:1")
    assert not check_acl(
        acl,
        "context.validate_supplier_invoice",
        "access_right:es_validate_supplier_invoice",
    )


def test_estimation_default_acls(estimation, acl_request, user):
    # Draft acls
    acl = get_estimation_default_acl(estimation)
    # User
    for ace in (
        "context.set_wait_estimation",
        "context.edit_estimation",
        "context.delete_estimation",
        "context.set_draft_estimation",
        "context.add_file",
        "context.view_file",
    ):
        assert check_acl(acl, ace, "company:1")
    assert not check_acl(acl, "context.validate_estimation", "company:1")

    assert not check_acl(acl, "context.geninv_estimation", "company:1")
    assert not check_acl(acl, "context.set_signed_status_estimation", "company:1")

    # # Admins
    for access_right in (
        "access_right:global_company_supervisor",
        "access_right:global_validate_estimation",
    ):
        for ace in (
            "context.edit_estimation",
            "context.delete_estimation",
            "context.set_draft_estimation",
            "context.add_file",
            "context.view_file",
        ):
            assert check_acl(acl, ace, access_right)

        assert check_acl(acl, "context.set_wait_estimation", access_right)

        assert not check_acl(acl, "context.geninv_estimation", access_right)
        assert not check_acl(acl, "context.set_signed_status_estimation", access_right)

    assert check_acl(
        acl, "context.validate_estimation", "access_right:global_validate_estimation"
    )
    # Auto validation draft status
    assert check_acl(
        acl, "context.validate_estimation", "access_right:es_validate_estimation"
    )
    assert check_acl(
        acl, "context.edit_estimation", "access_right:es_validate_estimation"
    )

    # Invalid acls
    estimation.status = "invalid"
    acl = get_estimation_default_acl(estimation)
    # User
    for ace in (
        "context.set_wait_estimation",
        "context.edit_estimation",
        "context.delete_estimation",
        "context.set_draft_estimation",
        "context.add_file",
        "context.view_file",
    ):
        assert check_acl(acl, ace, "company:1")
    assert not check_acl(acl, "context.validate_estimation", "company:1")

    assert not check_acl(acl, "context.geninv_estimation", "company:1")
    assert not check_acl(acl, "context.set_signed_status_estimation", "company:1")

    # # Admins && groups
    for access_right in (
        "access_right:global_company_supervisor",
        "access_right:global_validate_estimation",
    ):
        for ace in (
            "context.edit_estimation",
            "context.delete_estimation",
            "context.set_draft_estimation",
            "context.add_file",
            "context.view_file",
        ):
            assert check_acl(acl, ace, access_right)

        assert check_acl(acl, "context.set_wait_estimation", access_right)

        assert not check_acl(acl, "context.geninv_estimation", access_right)
        assert not check_acl(acl, "context.set_signed_status_estimation", access_right)

    assert check_acl(
        acl, "context.validate_estimation", "access_right:global_validate_estimation"
    )
    # Auto validation invalid status
    assert check_acl(
        acl, "context.validate_estimation", "access_right:es_validate_estimation"
    )
    assert check_acl(
        acl, "context.edit_estimation", "access_right:es_validate_estimation"
    )

    # Wait acls
    estimation.status = "wait"
    acl = get_estimation_default_acl(estimation)
    # #  User
    assert check_acl(acl, "context.set_draft_estimation", "company:1")
    assert not check_acl(acl, "context.set_wait_estimation", "company:1")
    assert not check_acl(acl, "context.edit_estimation", "company:1")
    assert not check_acl(acl, "context.validate_estimation", "company:1")

    assert not check_acl(acl, "context.geninv_estimation", "company:1")
    assert not check_acl(acl, "context.set_signed_status_estimation", "company:1")

    # EA
    access_right = "access_right:global_company_supervisor"
    assert not check_acl(acl, "context.validate_estimation", access_right)
    assert not check_acl(acl, "context.invalidate_estimation", access_right)
    assert not check_acl(acl, "context.edit_estimation", access_right)
    assert not check_acl(acl, "context.delete_estimation", access_right)
    assert check_acl(acl, "context.set_draft_estimation", access_right)
    assert not check_acl(acl, "context.geninv_estimation", access_right)
    assert not check_acl(acl, "context.set_signed_status_estimation", access_right)

    # EA avec le rôle de validation
    access_right = "access_right:global_validate_estimation"
    assert check_acl(acl, "context.validate_estimation", access_right)
    assert check_acl(acl, "context.invalidate_estimation", access_right)
    assert check_acl(acl, "context.edit_estimation", access_right)
    assert check_acl(acl, "context.delete_estimation", access_right)
    assert check_acl(acl, "context.set_draft_estimation", "company:1")
    assert not check_acl(acl, "context.geninv_estimation", access_right)
    assert not check_acl(acl, "context.set_signed_status_estimation", access_right)

    # Auto validation wait status
    assert check_acl(
        acl, "context.validate_estimation", "access_right:es_validate_estimation"
    )
    assert check_acl(
        acl, "context.edit_estimation", "access_right:es_validate_estimation"
    )

    # Valid acls
    estimation.status = "valid"
    acl = get_estimation_default_acl(estimation)
    # # User
    assert not check_acl(acl, "context.edit_estimation", "company:1")
    assert not check_acl(acl, "context.delete_estimation", "company:1")

    assert check_acl(acl, "context.geninv_estimation", "company:1")
    assert check_acl(acl, "context.set_signed_status_estimation", "company:1")

    # # Admins
    for access_right in (
        "access_right:global_company_supervisor",
        "access_right:global_validate_estimation",
    ):
        assert not check_acl(acl, "context.edit_estimation", access_right)
        assert not check_acl(acl, "context.delete_estimation", access_right)

        assert check_acl(acl, "context.geninv_estimation", access_right)
        assert check_acl(acl, "context.set_signed_status_estimation", access_right)

    # Aborted acls
    estimation.signed_status = "aborted"

    acl = get_estimation_default_acl(estimation)
    # # User
    assert not check_acl(acl, "context.geninv_estimation", "company:1")
    assert check_acl(acl, "context.set_signed_status_estimation", "company:1")

    # # Admins
    for access_right in (
        "access_right:global_company_supervisor",
        "access_right:global_validate_estimation",
    ):
        assert not check_acl(acl, "context.geninv_estimation", access_right)
        assert check_acl(acl, "context.set_signed_status_estimation", access_right)

    # Auto validation valid status
    assert not check_acl(
        acl, "context.validate_estimation", "access_right:es_validate_estimation"
    )
    assert not check_acl(
        acl, "context.edit_estimation", "access_right:es_validate_estimation"
    )

    # Signed acls
    estimation.signed_status = "signed"
    acl = get_estimation_default_acl(estimation)
    # # User
    assert check_acl(acl, "context.geninv_estimation", "company:1")
    assert check_acl(acl, "context.set_signed_status_estimation", "company:1")

    # # Admins
    for access_right in (
        "access_right:global_company_supervisor",
        "access_right:global_validate_estimation",
    ):
        assert check_acl(acl, "context.geninv_estimation", access_right)
        assert check_acl(acl, "context.set_signed_status_estimation", access_right)

    # geninv acls
    estimation.signed_status = "waiting"
    estimation.geninv = True
    acl = get_estimation_default_acl(estimation)
    # # User
    assert check_acl(acl, "context.geninv_estimation", "company:1")
    assert check_acl(acl, "context.set_signed_status_estimation", "company:1")

    # # Admins
    for access_right in (
        "access_right:global_company_supervisor",
        "access_right:global_validate_estimation",
    ):
        assert check_acl(acl, "context.geninv_estimation", access_right)
        assert check_acl(acl, "context.set_signed_status_estimation", access_right)


def test_estimation_exceed_limit_amount(
    estimation_exceed_limit_amount, acl_request, user, login
):
    login.estimation_limit_amount = 100.0

    estimation_exceed_limit_amount.status = "draft"
    acl = get_estimation_default_acl(estimation_exceed_limit_amount)

    assert check_acl(
        acl, "context.edit_estimation", "access_right:es_validate_estimation"
    )
    assert not check_acl(
        acl, "context.validate_estimation", "access_right:es_validate_estimation"
    )

    estimation_exceed_limit_amount.status = "invalid"
    acl = get_estimation_default_acl(estimation_exceed_limit_amount)

    assert check_acl(
        acl, "context.edit_estimation", "access_right:es_validate_estimation"
    )
    assert not check_acl(
        acl, "context.validate_estimation", "access_right:es_validate_estimation"
    )

    estimation_exceed_limit_amount.status = "wait"
    acl = get_estimation_default_acl(estimation_exceed_limit_amount)

    assert check_acl(
        acl, "context.edit_estimation", "access_right:es_validate_estimation"
    )
    assert not check_acl(
        acl, "context.validate_estimation", "access_right:es_validate_estimation"
    )

    estimation_exceed_limit_amount.status = "valid"
    acl = get_estimation_default_acl(estimation_exceed_limit_amount)

    assert not check_acl(
        acl, "context.edit_estimation", "access_right:es_validate_estimation"
    )
    assert not check_acl(
        acl, "context.validate_estimation", "access_right:es_validate_estimation"
    )


def test_invoice_default_acls(invoice, acl_request):
    # Draft acls
    acl = get_invoice_default_acl(invoice)
    # User
    # status related acl
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for ace in (
            "context.set_wait_invoice",
            "context.edit_invoice",
            "context.delete_invoice",
            "context.view_file",
            "context.add_file",
        ):
            assert check_acl(acl, ace, principal)
        assert not check_acl(acl, "context.validate_invoice", principal)
    # specific acl
    assert not check_acl(acl, "context.gen_cancelinvoice_invoice", "company:1")
    assert not check_acl(acl, "context.add_payment_invoice", "company:1")

    # Admins
    access_right = "access_right:global_validate_invoice"
    for ace in (
        "context.edit_invoice",
        "context.delete_invoice",
        "context.view_file",
        "context.add_file",
    ):
        assert check_acl(acl, ace, access_right)

    assert check_acl(acl, "context.set_wait_invoice", access_right)
    assert not check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)
    assert not check_acl(acl, "context.add_payment_invoice", access_right)

    assert check_acl(
        acl, "context.validate_invoice", "access_right:global_validate_invoice"
    )

    # Auto validation draft status
    assert check_acl(
        acl, "context.validate_invoice", "access_right:es_validate_invoice"
    )
    assert check_acl(acl, "context.edit_invoice", "access_right:es_validate_invoice")

    # Wait acls
    invoice.status = "wait"
    acl = get_invoice_default_acl(invoice)
    # #  User
    # assert check_acl(acl, "company.view", "company:1")
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for ace in (
            "context.edit_invoice",
            "context.delete_invoice",
            "context.validate_invoice",
            "context.gen_cancelinvoice_invoice",
            "context.add_payment_invoice",
        ):
            assert not check_acl(acl, ace, principal)
        assert check_acl(acl, "context.add_file", principal)

    access_right = ("access_right:global_validate_invoice",)
    assert check_acl(acl, "context.add_file", access_right)
    assert check_acl(acl, "context.edit_invoice", access_right)
    assert check_acl(acl, "context.delete_invoice", access_right)
    assert check_acl(acl, "context.validate_invoice", access_right)
    assert not check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)
    assert not check_acl(acl, "context.add_payment_invoice", access_right)

    # Auto validation wait status
    assert check_acl(
        acl, "context.validate_invoice", "access_right:es_validate_invoice"
    )
    assert check_acl(acl, "context.edit_invoice", "access_right:es_validate_invoice")

    # VALID INVOICE
    for status in "valid", "paid":
        if status == "valid":
            invoice.status = status
        if status == "paid":
            invoice.status = "valid"
            invoice.paid_status = "paid"

        acl = get_invoice_default_acl(invoice)

        for principal in ("company:1", "access_right:global_company_supervisor"):
            for ace in (
                "context.delete_invoice",
                "context.edit_invoice",
                "context.add_payment_invoice",
            ):
                assert not check_acl(acl, ace, principal)
            # assert check_acl(acl, "company.view", principal)
            assert check_acl(acl, "context.gen_cancelinvoice_invoice", principal)
            assert check_acl(acl, "context.add_file", principal)

        # # Admins
        access_right = "access_right:global_validate_invoice"
        assert not check_acl(acl, "context.edit_invoice", access_right)
        assert not check_acl(acl, "context.delete_invoice", access_right)
        assert not check_acl(acl, "context.add_payment_invoice", access_right)
        # assert check_acl(acl, "company.view", access_right)
        assert check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)
        assert check_acl(acl, "context.add_file", access_right)

        access_right = "access_right:global_record_payment_invoice"
        assert check_acl(acl, "context.add_payment_invoice", access_right)
        assert check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)

        access_right = "access_right:global_accountant"
        assert check_acl(acl, "context.add_payment_invoice", access_right)
        assert check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)

        # Auto validation valid status
        assert not check_acl(
            acl, "context.validate_invoice", "access_right:es_validate_estimation"
        )
        assert not check_acl(
            acl, "context.edit_invoice", "access_right:es_validate_estimation"
        )
        # Enregistrement de paiement
        assert check_acl(
            acl, "context.add_payment_invoice", "access_right:es_record_payment_invoice"
        )

    # RESULTED INVOICE
    invoice.paid_status = "resulted"
    acl = get_invoice_default_acl(invoice)
    # # User
    assert not check_acl(acl, "context.gen_cancelinvoice_invoice", "company:1")

    # equipe d'appui
    assert not check_acl(
        acl,
        "context.gen_cancelinvoice_invoice",
        "access_right:global_company_supervisor",
    )

    access_right = "access_right:global_validate_invoice"
    assert not check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)
    assert not check_acl(acl, "context.add_payment_invoice", access_right)

    access_right = "access_right:global_record_payment_invoice"
    assert check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)
    assert not check_acl(acl, "context.add_payment_invoice", access_right)
    access_right = "access_right:global_accountant"
    assert check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)

    # Drois spécifiques entrepreneurs
    assert check_acl(
        acl,
        "context.gen_cancelinvoice_invoice",
        "access_right:es_cancel_resulted_invoice",
    )


def test_invoice_exceed_limit_amount(invoice_exceed_limit_amount, acl_request, login):
    login.invoice_limit_amount = 100.0

    invoice_exceed_limit_amount.status = "draft"
    acl = get_invoice_default_acl(invoice_exceed_limit_amount)

    assert check_acl(acl, "context.edit_invoice", "access_right:es_validate_invoice")
    assert not check_acl(
        acl, "context.validate_invoice", "access_right:es_validate_invoice"
    )

    invoice_exceed_limit_amount.status = "invalid"
    acl = get_invoice_default_acl(invoice_exceed_limit_amount)

    assert check_acl(acl, "context.edit_invoice", "access_right:es_validate_invoice")
    assert not check_acl(
        acl, "context.validate_invoice", "access_right:es_validate_invoice"
    )

    invoice_exceed_limit_amount.status = "wait"
    acl = get_invoice_default_acl(invoice_exceed_limit_amount)

    assert check_acl(acl, "context.edit_invoice", "access_right:es_validate_invoice")
    assert not check_acl(
        acl, "context.validate_invoice", "access_right:es_validate_invoice"
    )

    invoice_exceed_limit_amount.status = "valid"
    acl = get_invoice_default_acl(invoice_exceed_limit_amount)

    assert not check_acl(
        acl, "context.edit_invoice", "access_right:es_validate_invoice"
    )
    assert not check_acl(
        acl, "context.validate_invoice", "access_right:es_validate_invoice"
    )


def test_internalinvoice_default_acls(internalinvoice, acl_request):
    from caerp.utils.security.acls import get_invoice_default_acl

    # Draft acls
    acl = get_invoice_default_acl(internalinvoice)
    # User
    # status related acl
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for ace in (
            "context.set_wait_invoice",
            "context.edit_invoice",
            "context.delete_invoice",
            "context.view_file",
            "context.add_file",
        ):
            assert check_acl(acl, ace, principal)
        assert not check_acl(acl, "context.validate_invoice", principal)
    # specific acl
    assert not check_acl(acl, "context.gen_cancelinvoice_invoice", "company:1")
    assert not check_acl(acl, "context.add_payment_invoice", "company:1")
    assert not check_acl(acl, "context.gen_supplier_invoice_invoice", "company:1")

    # # Admins
    access_right = "access_right:global_validate_invoice"
    for ace in (
        "context.edit_invoice",
        "context.delete_invoice",
        "context.view_file",
        "context.add_file",
    ):
        assert check_acl(acl, ace, access_right)
    assert check_acl(acl, "context.set_wait_invoice", access_right)
    assert not check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)
    assert not check_acl(acl, "context.add_payment_invoice", access_right)

    assert check_acl(
        acl, "context.validate_invoice", "access_right:es_validate_invoice"
    )
    # Wait acls
    internalinvoice.status = "wait"
    acl = get_invoice_default_acl(internalinvoice)

    # assert check_acl(acl, "company.view", "company:1")
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for ace in (
            "context.edit_invoice",
            "context.gen_cancelinvoice_invoice",
            "context.validate_invoice",
            "context.add_payment_invoice",
        ):
            assert not check_acl(acl, ace, principal)
        assert check_acl(acl, "context.add_file", principal)

    # # Equipe d'appui
    access_right = "access_right:global_validate_invoice"
    for ace in (
        "context.edit_invoice",
        "context.delete_invoice",
        "context.view_file",
        "context.add_file",
        "context.validate_invoice",
    ):
        assert check_acl(acl, ace, access_right)

    assert not check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)
    assert not check_acl(acl, "context.add_payment_invoice", access_right)

    # Valid acls
    internalinvoice.status = "valid"
    acl = get_invoice_default_acl(internalinvoice)
    # # User
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for ace in (
            "context.edit_invoice",
            "context.delete_invoice",
            "context.gen_cancelinvoice_invoice",
            "context.add_payment_invoice",
        ):
            assert not check_acl(acl, ace, principal)
        assert check_acl(acl, "context.add_file", principal)
    assert not check_acl(
        acl, "context.add_payment_invoice", "access_right:es_record_payment_invoice"
    )

    # # Admins
    access_right = "access_right:global_validate_invoice"
    assert not check_acl(acl, "context.edit_invoice", access_right)
    assert not check_acl(acl, "context.delete_invoice", access_right)
    assert check_acl(acl, "context.add_file", access_right)
    # assert check_acl(acl, "company.view", access_right)
    assert not check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)

    for access_right in (
        "access_right:global_accountant",
        "access_right:global_validate_invoice",
        "access_right:global_company_supervisor",
        "access_right:global_record_payment_invoice",
    ):
        # Ref #3501
        assert not check_acl(acl, "context.gen_supplier_invoice_invoice", access_right)

    internalinvoice.status_date -= datetime.timedelta(minutes=2)
    acl = get_invoice_default_acl(internalinvoice)
    for access_right in (
        "access_right:global_accountant",
        "access_right:global_validate_invoice",
        "access_right:global_company_supervisor",
        "access_right:global_record_payment_invoice",
    ):
        assert check_acl(acl, "context.gen_supplier_invoice_invoice", access_right)

    internalinvoice.supplier_invoice_id = 1
    acl = get_invoice_default_acl(internalinvoice)
    for access_right in (
        "access_right:global_record_payment_invoice",
        "access_right:global_validate_invoice",
    ):
        assert not check_acl(acl, "context.gen_supplier_invoice_invoice", access_right)

    # Paid acls
    internalinvoice.paid_status = "paid"

    acl = get_invoice_default_acl(internalinvoice)
    # # User
    assert not check_acl(acl, "context.gen_cancelinvoice_invoice", "company:1")
    assert check_acl(acl, "context.add_file", "company:1")

    # # Admins
    for access_right in (
        "access_right:global_record_payment_invoice",
        "access_right:global_accountant",
    ):
        assert not check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)
        assert check_acl(acl, "context.add_payment_invoice", access_right)

    # Resulted acls
    internalinvoice.paid_status = "resulted"
    acl = get_invoice_default_acl(internalinvoice)
    # # User
    assert not check_acl(acl, "context.gen_cancelinvoice_invoice", "company:1")

    # # Admins
    for access_right in (
        "access_right:global_record_payment_invoice",
        "access_right:global_accountant",
    ):
        assert not check_acl(acl, "context.gen_cancelinvoice_invoice", access_right)
        assert not check_acl(acl, "context.add_payment_invoice", access_right)


def test_cancelinvoice_default_acls(cancelinvoice):
    from caerp.utils.security.acls import get_cancelinvoice_default_acl

    # Draft acls
    acl = get_cancelinvoice_default_acl(cancelinvoice)

    for principal in ("company:1", "access_right:global_company_supervisor"):
        for ace in (
            "context.set_wait_cancelinvoice",
            "context.edit_cancelinvoice",
            "context.delete_cancelinvoice",
            "context.view_file",
            "context.add_file",
        ):
            assert check_acl(acl, ace, principal)
        assert not check_acl(acl, "context.validate_cancelinvoice", principal)
        assert not check_acl(acl, "context.duplicate_invoice", principal)

    access_right = "access_right:global_validate_cancelinvoice"
    for ace in (
        "context.edit_cancelinvoice",
        "context.delete_cancelinvoice",
        "context.view_file",
        "context.add_file",
    ):
        assert check_acl(acl, ace, access_right)

    assert check_acl(acl, "context.validate_cancelinvoice", access_right)
    assert check_acl(acl, "context.set_wait_cancelinvoice", access_right)
    assert not check_acl(acl, "context.duplicate_invoice", access_right)

    assert not check_acl(
        acl, "context.validate_cancelinvoice", "access_right:es_validate_invoice"
    )
    assert check_acl(
        acl, "context.validate_cancelinvoice", "access_right:es_validate_cancelinvoice"
    )

    # Wait acls
    cancelinvoice.status = "wait"
    acl = get_cancelinvoice_default_acl(cancelinvoice)
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for perm in (
            "context.edit_cancelinvoice",
            "set_date.cancelinvoice",
            "context.validate_cancelinvoice",
            "context.duplicate_invoice",
        ):
            assert not check_acl(acl, perm, principal)
        assert check_acl(acl, "context.add_file", principal)

    access_right = "access_right:global_validate_cancelinvoice"
    for ace in (
        "context.edit_cancelinvoice",
        "context.delete_cancelinvoice",
        "context.validate_cancelinvoice",
        "context.add_file",
    ):
        assert check_acl(acl, ace, access_right)
    assert not check_acl(acl, "context.duplicate_invoice", access_right)

    # VALID CANCELINVOICE
    cancelinvoice.status = "valid"
    acl = get_cancelinvoice_default_acl(cancelinvoice)
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for perm in (
            "context.edit_cancelinvoice",
            "set_date.cancelinvoice",
            "context.delete_cancelinvoice",
            "context.duplicate_invoice",
        ):
            assert not check_acl(acl, perm, principal)
        assert check_acl(acl, "context.add_file", principal)
    # # Admins
    access_right = "access_right:global_validate_cancelinvoice"
    for perm in (
        "context.edit_cancelinvoice",
        "context.delete_cancelinvoice",
        "context.duplicate_invoice",
    ):
        assert not check_acl(acl, perm, access_right)
    assert check_acl(acl, "context.add_file", access_right)

    access_right = "access_right:global_accountant"
    assert check_acl(acl, "context.set_treasury_cancelinvoice", access_right)


def test_internalcancelinvoice_default_acls(internalcancelinvoice, acl_request):
    from caerp.utils.security.acls import get_cancelinvoice_default_acl

    # Draft acls
    acl = get_cancelinvoice_default_acl(internalcancelinvoice)
    # User
    # status related acl
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for ace in (
            "context.set_wait_cancelinvoice",
            "context.edit_cancelinvoice",
            "context.delete_cancelinvoice",
            "context.view_file",
            "context.add_file",
        ):
            assert check_acl(acl, ace, principal)
        assert not check_acl(acl, "context.validate_cancelinvoice", principal)
    # specific acl
    assert not check_acl(acl, "context.gen_supplier_invoice_invoice", "company:1")

    # # Admins
    access_right = "access_right:global_validate_cancelinvoice"
    for ace in (
        "context.edit_cancelinvoice",
        "context.delete_cancelinvoice",
        "context.view_file",
        "context.add_file",
    ):
        assert check_acl(acl, ace, access_right)
    assert check_acl(acl, "context.set_wait_cancelinvoice", access_right)

    assert check_acl(
        acl, "context.validate_cancelinvoice", "access_right:es_validate_cancelinvoice"
    )
    # Wait acls
    internalcancelinvoice.status = "wait"
    acl = get_cancelinvoice_default_acl(internalcancelinvoice)

    # assert check_acl(acl, "company.view", "company:1")
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for ace in (
            "context.edit_cancelinvoice",
            "context.validate_cancelinvoice",
        ):
            assert not check_acl(acl, ace, principal)
        assert check_acl(acl, "context.add_file", principal)

    # # Equipe d'appui
    access_right = "access_right:global_validate_cancelinvoice"
    for ace in (
        "context.edit_cancelinvoice",
        "context.delete_cancelinvoice",
        "context.view_file",
        "context.add_file",
        "context.validate_cancelinvoice",
    ):
        assert check_acl(acl, ace, access_right)

    # Valid acls
    internalcancelinvoice.status = "valid"
    acl = get_cancelinvoice_default_acl(internalcancelinvoice)
    # # User
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for ace in (
            "context.edit_cancelinvoice",
            "context.delete_cancelinvoice",
        ):
            assert not check_acl(acl, ace, principal)
        assert check_acl(acl, "context.add_file", principal)

    # # Admins
    access_right = "access_right:global_validate_cancelinvoice"
    assert not check_acl(acl, "context.edit_cancelinvoice", access_right)
    assert not check_acl(acl, "context.delete_cancelinvoice", access_right)
    assert check_acl(acl, "context.add_file", access_right)

    for access_right in (
        "access_right:global_accountant",
        "access_right:global_validate_cancelinvoice",
        "access_right:global_company_supervisor",
    ):
        # Ref #3501
        assert not check_acl(acl, "context.gen_supplier_invoice_invoice", access_right)

    internalcancelinvoice.status_date -= datetime.timedelta(minutes=2)
    acl = get_cancelinvoice_default_acl(internalcancelinvoice)
    for access_right in ("access_right:global_validate_cancelinvoice",):
        assert check_acl(acl, "context.gen_supplier_invoice_invoice", access_right)

    internalcancelinvoice.supplier_invoice_id = 1
    acl = get_cancelinvoice_default_acl(internalcancelinvoice)
    for access_right in ("access_right:global_validate_cancelinvoice",):
        assert not check_acl(acl, "context.gen_supplier_invoice_invoice", access_right)


def test_expense_sheet_default_acls(expense_sheet):
    from caerp.utils.security.acls import get_expense_sheet_default_acl

    acl = get_expense_sheet_default_acl(expense_sheet)

    # User
    # status related acl
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for ace in (
            "context.edit_expensesheet",
            "context.delete_expensesheet",
            "context.view_file",
            "context.add_file",
            "context.set_wait_expensesheet",
        ):
            assert check_acl(acl, ace, principal)
        assert not check_acl(acl, "context.validate_expensesheet", principal)
        assert not check_acl(acl, "context.add_payment_expensesheet", principal)

    # # Admins
    access_right = "access_right:global_validate_expensesheet"

    for ace in (
        "context.edit_expensesheet",
        "context.delete_expensesheet",
        "context.view_file",
        "context.add_file",
        "context.set_wait_expensesheet",
        "context.validate_expensesheet",
    ):
        assert check_acl(acl, ace, access_right)

    assert not check_acl(acl, "context.add_payment_expensesheet", access_right)

    # Wait acls
    expense_sheet.status = "wait"
    acl = get_expense_sheet_default_acl(expense_sheet)
    # #  User
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for ace in (
            "context.edit_expensesheet",
            "context.valid_expensesheet",
            "context.validate_expensesheet",
            "context.add_payment_expensesheet",
        ):
            assert not check_acl(acl, ace, principal)
        assert check_acl(acl, "context.set_draft_expensesheet", principal)
        assert check_acl(acl, "context.add_file", principal)

    # # Admins
    access_right = "access_right:global_validate_expensesheet"
    assert check_acl(acl, "context.edit_expensesheet", access_right)
    assert check_acl(acl, "context.delete_expensesheet", access_right)
    assert check_acl(acl, "context.add_file", access_right)
    assert check_acl(acl, "context.validate_expensesheet", access_right)
    assert check_acl(acl, "context.set_justified_expensesheet", access_right)
    assert not check_acl(acl, "context.add_payment_expensesheet", access_right)

    # Valid acls
    expense_sheet.status = "valid"
    acl = get_expense_sheet_default_acl(expense_sheet)
    for principal in ("company:1", "access_right:global_company_supervisor"):
        for ace in (
            "context.edit_expensesheet",
            "context.valid_expensesheet",
            "context.set_draft_expensesheet",
            "context.validate_expensesheet",
            "context.add_payment_expensesheet",
        ):
            assert not check_acl(acl, ace, principal)

        assert check_acl(acl, "context.add_file", principal)
    access_right = "access_right:global_validate_expensesheet"
    for ace in (
        "context.edit_expensesheet",
        "context.valid_expensesheet",
        "context.set_draft_expensesheet",
        "context.validate_expensesheet",
    ):
        assert not check_acl(acl, ace, access_right)

    assert check_acl(acl, "context.add_file", access_right)
    assert not check_acl(acl, "context.add_payment_expensesheet", access_right)
    assert check_acl(acl, "context.set_justified_expensesheet", access_right)
    access_right = "access_right:global_record_payment_expensesheet"
    assert check_acl(acl, "context.add_payment_expensesheet", access_right)

    # Paid acls
    expense_sheet.paid_status = "paid"
    acl = get_expense_sheet_default_acl(expense_sheet)
    # # User
    assert check_acl(acl, "context.add_file", "company:1")
    assert check_acl(acl, "context.add_file", "access_right:global_company_supervisor")

    # # Admins
    access_right = "access_right:global_validate_expensesheet"
    assert check_acl(acl, "context.add_file", access_right)
    assert not check_acl(acl, "context.add_payment_expensesheet", access_right)

    # Resulted acls
    expense_sheet.paid_status = "resulted"
    acl = get_expense_sheet_default_acl(expense_sheet)

    # # Admins
    access_right = "access_right:global_validate_expensesheet"
    assert not check_acl(acl, "context.add_payment_expensesheet", access_right)
    assert check_acl(acl, "context.add_file", access_right)

    access_right = "global_record_payment_expensesheet"
    assert not check_acl(acl, "context.add_payment_expensesheet", access_right)


def test_statuslogentry_acls(mk_status_log_entry, user, login, node):
    from caerp.utils.security.acls import get_statuslogentry_acl

    company_id = 42
    node.company_id = company_id

    kwargs = dict(user_id=user.id, node_id=node.id)
    private_sle_acl = get_statuslogentry_acl(
        mk_status_log_entry(visibility="private", **kwargs)
    )
    public_sle_acl = get_statuslogentry_acl(
        mk_status_log_entry(visibility="public", **kwargs)
    )
    manager_sle_acl = get_statuslogentry_acl(
        mk_status_log_entry(visibility="manager", **kwargs)
    )

    view = "context.view_statuslogentry"
    edit = "context.edit_statuslogentry"
    delete = "context.delete_statuslogentry"

    # author
    for acl in (private_sle_acl, public_sle_acl, manager_sle_acl):
        assert check_acl(acl, [view, edit, delete], user.login.login)

    # admin
    for acl in (private_sle_acl, public_sle_acl, manager_sle_acl):
        assert check_acl(
            acl, [view, edit, delete], "access_right:global_config_company"
        )

    # manager (EA)
    for acl in (public_sle_acl, manager_sle_acl):
        assert check_acl(acl, view, "access_right:global_company_supervisor")
        assert not check_acl(
            acl, [edit, delete], "access_right:global_company_supervisor"
        )

    assert not check_acl(
        private_sle_acl, [view, edit, delete], "access_right:global_company_supervisor"
    )

    # company
    assert check_acl(public_sle_acl, view, f"company:{company_id}")
    assert not check_acl(public_sle_acl, [edit, delete], f"company:{company_id}")
    for acl in (private_sle_acl, manager_sle_acl):
        assert not check_acl(acl, [view, edit, delete], f"company:{company_id}")
