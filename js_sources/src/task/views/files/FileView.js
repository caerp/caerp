import Mn from "backbone.marionette"
import Radio from "backbone.radio"

const template = require("./templates/FileView.mustache")

const FileView = Mn.View.extend({
    tagName: "tr",
    template: template,
    ui: {
        edit_button: ".btn-edit",
        view_button: ".btn-view",
        clickable_td: "td.clickable",
    },
    events: {
        "click @ui.view_button": "onView",
        "click @ui.clickable_td": "onView",
        "click @ui.edit_button": "onEdit",
    },
    modelEvents: {
        "change:name": "render",
    },
    onFilePopupCallback(options) {
        this.triggerMethod("file:updated")
    },
    onView() {
        Radio.channel("facade").trigger(
            "show:preview",
            this.model,
            "Fichier Facultatif"
        )
    },
    onEdit() {
        window.openPopup(
            "/files/" + this.model.get("id"),
            this.onFilePopupCallback.bind(this)
        )
    },
    isPreviewable() {
        return Radio.channel("config").request("is:previewable", this.model)
    },
    templateContext() {
        return {
            description_differs:
                this.model.get("description") !== this.model.get("name"),
            is_previewable: this.isPreviewable(),
        }
    },
})
export default FileView
