def format_boolean(value):
    """
    Format a boolean value
    """
    return value and "Y" or "N"
