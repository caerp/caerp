import _ from "underscore"
import Bb from "backbone"

import ExpenseTypeModel from "./ExpenseTypeModel.js"

const ExpenseTypeCollection = Bb.Collection.extend({
    model: ExpenseTypeModel,
})
export default ExpenseTypeCollection
