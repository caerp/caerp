/*
 * Module name : WorkItemView
 */
import Mn from "backbone.marionette"
import Radio from "backbone.radio"

import ButtonModel from "base/models/ButtonModel.js"
import ButtonWidget from "widgets/ButtonWidget.js"

const template = require("./templates/WorkItemView.mustache")

const WorkItemView = Mn.View.extend({
    template: template,
    tagName: "tr",
    regions: {
        editButtonContainer: ".col_actions .edit",
        delButtonContainer: ".col_actions .delete",
    },
    childViewEvents: {
        "action:clicked": "onActionClicked",
    },
    modelEvents: {
        change: "render",
    },
    initialize() {
        this.config = Radio.channel("config")
        this.unity_options = this.config.request("get:options", "unities")
    },
    onRender() {
        let editModel = new ButtonModel({
            label: "Modifier cet élément",
            icon: "pen",
            showLabel: false,
            action: "edit",
        })
        let deleteModel = new ButtonModel({
            label: "Supprimer cet élément",
            icon: "trash-alt",
            showLabel: false,
            action: "delete",
            css: "negative",
        })
        this.showChildView(
            "editButtonContainer",
            new ButtonWidget({
                model: editModel,
            })
        )
        this.showChildView(
            "delButtonContainer",
            new ButtonWidget({
                model: deleteModel,
            })
        )
    },
    templateContext() {
        return {
            ht_label: this.model.ht_label(),
            supplier_ht_label: this.model.supplier_ht_label(),
            total_ht_label: this.model.total_ht_label(),
        }
    },
    onActionClicked(action) {
        if (action == "edit") {
            this.triggerMethod("model:edit", this.model, this)
        } else if (action == "delete") {
            this.triggerMethod("model:delete", this.model, this)
        }
    },
})
export default WorkItemView
