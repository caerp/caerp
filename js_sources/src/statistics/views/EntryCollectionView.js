import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import EntryView from "./EntryView"
import EntryEmptyView from "./EntryEmptyView"

const EntryCollectionView = Mn.CollectionView.extend({
    childView: EntryView,
    emptyView: EntryEmptyView,
    tagName: "tbody",
    childViewTriggers: {
        "model:edit": "model:edit",
        "model:delete": "model:delete",
        "model:export": "model:export",
        "model:duplicate": "model:duplicate",
    },
    initialize() {
        this.config = Radio.channel("config")
        this.facade = Radio.channel("facade")
    },
})
export default EntryCollectionView
