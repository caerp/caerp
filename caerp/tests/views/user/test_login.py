from caerp.tests.tools import DummyForm


class TestLoginAddView:
    def test_before(self, get_csrf_request_with_db, user):
        from caerp.views.user.login import LoginAddView

        req = get_csrf_request_with_db()
        req.context = user

        view = LoginAddView(req)
        form = DummyForm()
        view.before(form)
        assert form.appstruct["login"] == user.email
        assert form.appstruct["user_id"] == user.id

    def test_before_groups(self, config, get_csrf_request_with_db, user):
        from caerp.views.user.login import LoginAddView

        req = get_csrf_request_with_db()
        req.context = user
        req.session["user_form"] = {
            "defaults": {"groups": ["contractor"], "account_type": "entrepreneur"}
        }

        view = LoginAddView(req)
        form = DummyForm()
        view.before(form)
        assert form.appstruct["login"] == user.email
        assert form.appstruct["user_id"] == user.id
        assert form.appstruct["groups"] == ["contractor"]
        assert form.appstruct["account_type"] == "entrepreneur"

    def test_submit_success(
        self,
        config,
        get_csrf_request_with_db,
        user,
        groups,
    ):
        from caerp.views.user.login import LoginAddView
        from caerp.models.user.login import Login

        config.add_route("/users/{id}", "/users/{id}")
        req = get_csrf_request_with_db()
        req.context = user

        appstruct = {
            "pwd_hash": "password",
            "login": "test1@email.fr",
            "account_type": "entrepreneur",
            "groups": ["trainer"],
        }

        view = LoginAddView(req)
        result = view.submit_success(appstruct)
        new_login = Login.query().filter_by(login="test1@email.fr").one()
        assert result.code == 302
        assert result.location == "/users/{0}".format(user.id)

        assert new_login.groups == ["trainer"]
        assert new_login.account_type == "entrepreneur"
        assert new_login.auth("password")

    def test_submit_success_next_step(
        self,
        config,
        get_csrf_request_with_db,
        user,
        groups,
    ):
        from caerp.views.user.login import LoginAddView
        from caerp.models.user.login import Login

        config.add_route("/path1/{id}", "/path1/{id}")
        config.add_route("/path2/{id}", "/path2/{id}")
        req = get_csrf_request_with_db()
        req.context = user
        req.session["user_form"] = {"callback_urls": ["/path1/{id}", "/path2/{id}"]}

        appstruct = {"pwd_hash": "password", "login": "test1@email.fr"}

        view = LoginAddView(req)
        result = view.submit_success(appstruct)
        new_login = Login.query().filter_by(login="test1@email.fr").one()
        assert result.code == 302
        assert result.location == "/path2/{0}".format(user.id)
        assert req.session["user_form"]["callback_urls"] == ["/path1/{id}"]

        assert new_login.auth("password")


class TestUserLoginEditView:
    def get_view(self, get_csrf_request_with_db, user):
        from caerp.views.user.login import UserLoginEditView

        req = get_csrf_request_with_db()
        req.context = user
        return UserLoginEditView(req)

    def test_before(self, get_csrf_request_with_db, user, groups, login):

        login.invoice_limit_amount = 400.0

        view = self.get_view(get_csrf_request_with_db, user)
        form = DummyForm()
        view.before(form)
        assert form.appstruct["login"] == login.login
        assert form.appstruct["user_id"] == user.id
        assert form.appstruct["invoice_limit_amount"] == 400.0

    def test_submit_success(
        self, config, get_csrf_request_with_db, user, groups, login
    ):

        config.add_route("/users/{id}/login", "/users/{id}/login")

        appstruct = {
            "pwd_hash": "new_password",
            "login": "test1@email.fr",
            "invoice_limit_amount": 200.0,
            "estimation_limit_amount": -100.0,
        }
        view = self.get_view(get_csrf_request_with_db, user)
        result = view.submit_success(appstruct)
        assert result.code == 302
        assert result.location == "/users/{0}/login".format(user.id)
        assert login.login == "test1@email.fr"
        assert login.invoice_limit_amount == 200.0
        assert login.estimation_limit_amount == 100.0
        assert login.auth("new_password")

    def test_submit_success_no_password(
        self, config, get_csrf_request_with_db, user, groups, login
    ):

        config.add_route("/users/{id}/login", "/users/{id}/login")

        appstruct = {"pwd_hash": "", "login": "test1@email.fr"}
        view = self.get_view(get_csrf_request_with_db, user)
        result = view.submit_success(appstruct)
        assert result.code == 302
        assert result.location == "/users/{0}/login".format(user.id)
        assert login.login == "test1@email.fr"

        assert login.auth("pwd")


class TestUserLoginPasswordView:
    def get_view(self, get_csrf_request_with_db, user):
        from caerp.views.user.login import UserLoginPasswordView

        req = get_csrf_request_with_db()
        req.context = user
        return UserLoginPasswordView(req)

    def test_submit_success(self, config, get_csrf_request_with_db, user, login):

        config.add_route("/users/{id}/login", "/users/{id}/login")

        appstruct = {
            "pwd_hash": "new_password",
        }
        view = self.get_view(get_csrf_request_with_db, user)
        result = view.submit_success(appstruct)
        assert result.code == 302
        assert result.location == "/users/{0}/login".format(login.user_id)
        assert login.auth("new_password")

    def test_submit_success_unchanged(
        self, config, get_csrf_request_with_db, user, login
    ):
        config.add_route("/users/{id}/login", "/users/{id}/login")
        appstruct = {
            "pwd_hash": "",
        }
        view = self.get_view(get_csrf_request_with_db, user)
        result = view.submit_success(appstruct)
        assert result.code == 302
        assert result.location == "/users/{0}/login".format(login.user_id)
        # Not changed
        assert login.auth("pwd")


class TestUserLoginDisableView:
    def get_view(self, get_csrf_request_with_db, user):
        from caerp.views.user.login import UserLoginDisableView

        req = get_csrf_request_with_db()
        req.context = user
        return UserLoginDisableView(req)

    def test_disable(self, config, user, login, get_csrf_request_with_db):

        config.add_route("/users/{id}/login", "/users/{id}/login")

        view = self.get_view(get_csrf_request_with_db, user)
        result = view()
        assert result.code == 302
        assert result.location == "/users/{0}/login".format(login.user_id)
        assert not login.active
        # Now we reactivate again
        view()
        assert login.active

    def test_disable_with_company(
        self, config, user, login, company, get_csrf_request_with_db
    ):

        config.add_route("/users/{id}/login", "/users/{id}/login")

        view = self.get_view(get_csrf_request_with_db, login.user)
        result = view()
        assert result.code == 302
        assert result.location == "/users/{0}/login".format(login.user_id)
        assert not login.active
        assert not company.active
        # Now we reactivate again
        view()
        assert login.active


class TestLoginDeleteView:
    def get_view(self, get_csrf_request_with_db, user):
        from caerp.views.user.login import LoginDeleteView

        req = get_csrf_request_with_db()
        req.context = user
        return LoginDeleteView(req)

    def test_delete(self, config, user, groups, login, get_csrf_request_with_db):
        from caerp.models.user.login import Login

        config.add_route("/users/{id}", "/users/{id}")
        login_id = login.id
        view = self.get_view(get_csrf_request_with_db, user)
        result = view()
        assert result.code == 302
        assert result.location == "/users/{0}".format(user.id)
        view.dbsession.flush()
        assert Login.get(login_id) is None
