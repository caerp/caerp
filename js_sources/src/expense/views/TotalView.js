import Mn from "backbone.marionette"
import { formatPrice, formatAmount } from "../../math.js"

const TotalView = Mn.View.extend({
    tagName: "div",
    template: require("./templates/TotalView.mustache"),
    modelEvents: {
        "change:ttc": "render",
        "change:ht": "render",
        "change:tva": "render",
        "change:km_ht": "render",
        "change:km_tva": "render",
        "change:km_ttc": "render",
        "change:km": "render",
    },
    templateContext() {
        return {
            ht: formatAmount(this.model.get("ht") + this.model.get("km_ht")),
            tva: formatAmount(this.model.get("tva") + this.model.get("km_tva")),
            ttc: formatAmount(this.model.get("ttc") + this.model.get("km_ttc")),
            km: formatPrice(this.model.get("km")),
        }
    },
})
export default TotalView
