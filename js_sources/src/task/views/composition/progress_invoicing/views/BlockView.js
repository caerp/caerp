/*
 * Module name : BlockView
 */
import Mn from "backbone.marionette"
import Radio from "backbone.radio"

import ErrorView from "base/views/ErrorView.js"
import ChapterCollectionView from "./chapter/ChapterCollectionView.js"

const template = require("./templates/BlockView.mustache")

const BlockView = Mn.View.extend({
    template: template,
    regions: {
        products: ".products",
        errors: ".errors",
    },
    initialize(options) {
        this.facade = Radio.channel("facade")
        this.config = Radio.channel("config")
        this.section = options["section"]
    },
    showChapters() {
        const view = new ChapterCollectionView({
            collection: this.collection,
            childViewOptions: {
                edit: this.section["edit"],
                section: this.section["products"],
            },
        })
        this.showChildView("products", view)
    },
    onRender() {
        this.showChapters()
    },
    templateContext() {
        return {
            editable: this.section["edit"],
        }
    },
    formOk() {
        var result = true
        var errors = this.facade.request("is:valid")
        if (!_.isEmpty(errors)) {
            this.showChildView(
                "errors",
                new ErrorView({
                    errors: errors,
                })
            )
            result = false
        } else {
            this.detachChildView("errors")
        }
        return result
    },
})
export default BlockView
