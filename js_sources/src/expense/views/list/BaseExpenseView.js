import Mn from "backbone.marionette"

import BusinessLinkView from "common/views/BusinessLinkView.js"

/** Abstract Common base for ExpenseView and ExpenseKmView
 */
const BaseExpenseView = Mn.View.extend({
    tagName: "tr",

    modelEvents: {
        sync: "render",
    },
    regions: {
        businessLink: {
            el: ".business-link",
        },
    },
    isAchat() {
        return this.model.get("category") == 2
    },
    onRender() {
        if (this.isAchat()) {
            var view = new BusinessLinkView({
                customer_label: this.model.get("customer_label"),
                project_label: this.model.get("project_label"),
                business_label: this.model.get("business_label"),
            })
            this.showChildView("businessLink", view)
        }
    },
})
export default BaseExpenseView
