/*
 * File Name : ProductCollection.js
 */
import OrderableCollection from "base/models/OrderableCollection.js"
import ProductModel from "./ProductModel.js"
import WorkModel from "./WorkModel.js"
import Radio from "backbone.radio"
import _ from "underscore"

const ProductCollection = OrderableCollection.extend({
    model(modeldict, options) {
        if (modeldict.type_ == "price_study_work") {
            return new WorkModel(modeldict, options)
        } else {
            return new ProductModel(modeldict, options)
        }
    },
    HT: function () {
        var result = 0
        this.each(function (model) {
            result += model.ht()
        })
        return result
    },
    TVAParts: function () {
        var result = {}
        this.each(function (model) {
            var tva_amount = model.tva()
            var tva = model.tva_label()
            if (tva in result) {
                tva_amount += result[tva]
            }
            result[tva] = tva_amount
        })
        return result
    },
    TTC: function () {
        var result = 0
        this.each(function (model) {
            result += model.ttc()
        })
        return result
    },
    validate: function () {
        var result = {}
        if (this.models.length == 0) {
            result["products"] =
                "Étude: veuillez saisir au moins une prestation"
        }
        this.each(function (model) {
            var res = model.validateModel()
            console.log("We've got a validation error on one model")
            if (res && !_.isEmpty(res)) {
                Object.assign(result, {
                    products: res,
                })
            }
        })
        return result
    },
})
export default ProductCollection
