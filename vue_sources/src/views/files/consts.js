export const PROVIDE_KEY = 'SALE_FILES_PROVIDER'
export const FILE_REQUIREMENT_LABELS = {
    optionnal: 'Facultatif',
    recommended: 'Recommandé',
    mandatory: 'Requis pour la validation',
    business_mandatory: "Requis dans l'affaire pour la validation",
    project_mandatory: 'Requis dans le dossier pour la validation',
  }