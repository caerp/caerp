Le principe
------------------

Le module de requêtes statistiques permet de mettre facilement à disposition des utilisateurs (EA) le résultat de requêtes complexes.


Création d'une requête statistique
-----------------------------------

Pour créer une nouvelle requête il suffit de créer une classe héritant de ``BaseDataQuery`` et redéfinissant les attributs et méthodes ci-dessous puis de l'ajouter au registre (voir plus bas le paragraphe dédié) :

- ``name`` : le nom "technique" de la requête, utilisé comme identifiant
- ``label`` : le libellé de la requête, affiché aux utilisateurs
- ``description`` : le texte de description de la requête pour expliquer aux utilisateurs son but et son fonctionnement
- ``default_dates(self)`` : la méthode (optionnelle) permettant d'initialiser les paramètres de période de la requête (voir plus bas le paragraphe dédié)
- ``headers(self)`` : la méthode qui retourne les en-têtes du tableau de résultat de la requête, sous forme de liste
- ``data(self)`` : la méthode qui retourne le corps du tableau de résultat de la requête, sous la forme d'une liste de listes qui représentent chacune une ligne du tableau (le nombre d'items dans chaque liste doit correspondre à celui des en-têtes)

Exemple de code minimal d'une requête :

.. code-block:: python

    from caerp.dataqueries.base import BaseDataQuery


    class MaRequete(BaseDataQuery):

        name = "ma_requete"
        label = "Titre de ma requête"
        description = """
        Description de la requête
        """

        def headers(self):
            return ["col1", "col2", "col3"]

        def data(self):
            data = [
                ["lig1_col1", "lig1_col2", "lig1_col3"],
                ["lig2_col1", "lig2_col2", "lig2_col3"],
                ["lig3_col1", "lig3_col2", "lig3_col3"],
            ]
            return data


    def includeme(config):
        config.register_dataquery(MaRequete)


Gestion de la période des requêtes
-----------------------------------

Pour chaque requête il y a 3 possibilités :

- `Aucune date` : la requête est toujours calculée par rapport à la date du jour (ex: les infos à l'instant T)
- `Une date de début` : la requête est calculée par rapport à une date donnée (ex: les infos au 31/12 de l'année dernière)
- `Une date de début et une date de fin` : la requête est calculée par rapport à une période donnée (ex: les infos du 1er trimestre de l'année)

Ces possibilités sont gérées de façon facilitée par les attributs de classe ``start_date`` et ``end_date``. Par défaut ces 2 attributs sont à ``None``, ce qui correspond à une requête à l'instant T (pas de date). On peut configurer une requête pour être calculée par rapport à une date ou une période donnée en surchargeant la méthode ``default_dates(self)`` et en donnant une valeur par défaut aux attributs ``start_date`` et ``end_date`` (on peut utiliser la méthode ``self.set_dates()``). Plusieurs méthodes prédéfinies dans la classe ``DateTools`` sont disponibles (directement depuis ``self.date_tools`` grâce à l'héritage de ``BaseDataQuery``) pour simplifier la gestion de ces dates.

Exemple pour une requête calculée sur le mois dernier par défaut :

.. code-block:: python

    def default_dates(self):
        self.set_dates(
            start = self.date_tools.month_start(),
            end = self.date_tools.month_end(),
        )

Exemple pour une requête calculée à la fin de l'année dernière par défaut :

.. code-block:: python

    def default_dates(self):
        self.set_dates(start = self.date_tools.previous_year_end())

Ensuite les utilisateurs auront automatiquement accès à un formulaire pour modifier les valeurs définies par défaut.


Intégration des requêtes dans CAERP
------------------------------------

Il existe un registre des requêtes statistiques qui est attaché à la config Pyramid (objet ``config``) et qui centralise toutes les requêtes pour les utiliser dans CAERP.

Les requêtes qui sont dans stockées dans le package ``caerp.dataqueries.queries`` et décorées avec ``@dataquery_class()`` sont automatiquement ajoutées au registre (grâce au ``config.scan()`` qui s'appuie sur ce décorateur).

.. code-block:: python

    from caerp.dataqueries.base import BaseDataQuery
    from caerp.utils.dataqueries import dataquery_class

    @dataquery_class()
    class MaRequete(BaseDataQuery):
        [...]

Autrement il faut ajouter manuellement chaque requête au registre avec la fonction ``config.register_dataquery()`` qui prend en argument la classe de la requête.

.. code-block:: python

    config.register_dataquery(MaRequete1)
    config.register_dataquery(MaRequete2)


Test des requêtes pour le développement
----------------------------------------

Les requêtes sont générées dans un fichier d'export au format choisi par l'utilisateur, mais pour faciliter le développement il est possible d'afficher le résultat de la requête directement dans la vue CAERP en utilisant l'url suivant :

.. code-block:: console

    https://{mon-serveur-caerp}/dataqueries/{nom-de-ma-requete}?start={YYYY-MM-DD}&end={YYYY-MM-DD}&format=display

