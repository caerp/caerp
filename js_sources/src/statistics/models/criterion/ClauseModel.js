import BaseModel from "./BaseModel"

/*
Model representing a criteria clause of type "and" / "or"
Bound its (children) criteria following the type clause
*/

const ClauseModel = BaseModel.extend({
    // Define the props that should be set on your model
    props: ["children"],
    initialize() {
        this.populate()
    },
    populate() {
        if (this.has("children")) {
            let subpath = this.collection.path.concat([])
            if (this.has("key")) {
                subpath.push(this.get("key"))
            }
            this.children = new this.collection.__class__(
                this.get("children"),
                { path: subpath, url: this.collection.url }
            )
        }
        this.children._parent = this
        this.children.url = this.collection.url
    },
})
ClauseModel.prototype.props = ClauseModel.prototype.props.concat(
    BaseModel.prototype.props
)

export default ClauseModel
