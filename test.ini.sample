[app:caerp]
use = egg:caerp

pyramid.reload_assets=true
pyramid.default_locale_name = fr
pyramid.includes =  pyramid_tm
                    pyramid_mailer.testing
                    pyramid_services
                    pyramid_layout
                    pyramid_mako
                    pyramid_chameleon
                    pyramid_celery
                    js.deform
                    deform_extensions
                    caerp_celery

#### BEGIN STANDARD CONFIG BLOCK
# CONFIGURE THOSE LINES TO ALLOW THE TESTS TO SETUP A MYSQL DATABASE ####
sqlalchemy.url = mysql://testcaerp:password@localhost/testcaerp?charset=utf8mb4
caerp_payment_db.url = mysql://testcaerp:password@localhost/testcaerp?charset=utf8mb4

sqlalchemy.echo=False

testdb.connect=echo 'quit' | mysql -uroot
testdb.adduser=echo "CREATE USER testcaerp" | mysql -uroot
testdb.adddb=echo "CREATE DATABASE IF NOT EXISTS testcaerp; GRANT ALL PRIVILEGES on testcaerp.* to testcaerp@localhost IDENTIFIED BY 'password';FLUSH PRIVILEGES;" | mysql -uroot
testdb.drop=echo "DROP DATABASE IF EXISTS testcaerp"|mysql -uroot
#### END STANDARD CONFIG BLOCK

#### BEGIN DOCKER-COMPOSE CONFIG BLOCK
# Uncomment the following lines, and comment the standard config block to use
# the db server hosted on your VM if you are using docker-compose
# sqlalchemy.url = mysql://caerp:caerp@127.0.0.1:13306/testcaerp?charset=utf8mb4
# caerp_payment_db.url = mysql://caerp:caerp@127.0.0.1:13306/testcaerp?charset=utf8mb4
# sqlalchemy.echo=False

# testdb.connect=echo 'quit' | docker-compose -f db-docker-compose.yaml run --rm mariadb mysql -uroot -proot -P 3306 -hmariadb
# testdb.adddb=echo "CREATE DATABASE IF NOT EXISTS testcaerp; GRANT ALL PRIVILEGES on testcaerp.* to 'caerp';FLUSH PRIVILEGES;" | docker-compose -f db-docker-compose.yaml run --rm mariadb mysql -uroot -proot -P 3306 -hmariadb
# testdb.adduser=
# testdb.drop=echo "DROP DATABASE IF EXISTS testcaerp"| docker-compose -f db-docker-compose.yaml run --rm mariadb mysql -uroot -proot -P 3306 -hmariadb
#### END DOCKER-COMPOSE CONFIG BLOCK


session.longtimeout=3600
cache.regions = default_term, second, short_term, long_term
# Pour les tests unitaires, pas d'impact sur le temps de run
cache.enabled = False
cache.type = memory
cache.second.expire = 1
cache.short_term.expire = 1
cache.default_term.expire = 1
cache.long_term.expire = 1
mako.directories = caerp:templates
mako.imports = from markupsafe import escape_silent
mako.default_filters = escape_silent
caerp.ftpdir=%(here)s/caerp/tests/datas/
pyramid_deform.tempdir=%(here)s/caerp/tests/tmp
caerp.depot_path=%(here)s/caerp/tests/tmp/filedepot/
# payment specific configuration see https://framagit.org/caerp/caerp_payment
# public service
caerp.interfaces.ipaymentrecordservice = caerp_payment.public.paymentservice

# Payment specific configuration see https://framagit.org/caerp/caerp_payment
# History management service
caerp_payment.interfaces.IPaymentRecordHistoryService = caerp_payment.history.HistoryDBService

# History Archive service
caerp_payment.interfaces.IPaymentArchiveService = caerp_payment.archive.FileArchiveService
caerp_payment_archive_storage_path = %(here)s/caerp/tests/tmp/

caerp.includes=

[pipeline:main]
pipeline =
    caerp

# Begin logging configuration
[loggers]
keys = root

[handlers]
keys = console

[logger_root]
level = DEBUG
handlers = console
