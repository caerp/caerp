import BaseQualiopiKnowledgeSaleProduct from "./BaseQualiopiKnowledgeSaleProduct"

const TrainingModel = BaseQualiopiKnowledgeSaleProduct.extend({
    props: [
        "rncp_rs_code",
        "certification_date",
        "certification_name",
        "certificator_name",
        "gateways",
        "modality_one",
        "modality_two",
        "prerequisites",
    ],
})
TrainingModel.prototype.props = TrainingModel.prototype.props.concat(
    BaseQualiopiKnowledgeSaleProduct.prototype.props
)
export default TrainingModel
