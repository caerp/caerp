#!/bin/bash
echo "Resetting db"
if [ "$1" == '-f' ]
then
    echo "Forcing"
    MYSQLCMD='mysql -u root'
    if [ "$2" != "" ]
    then
        DBNAME=$2
    else
        DBNAME='caerp'
    fi
    DBUSER='caerp@localhost'
    DBPASS='caerp'
elif [ "$1" == "docker" ]
then
    echo "Docker mode"
    MYSQLCMD='mysql -uroot -P 3308 -h 127.0.0.1 -pmariadb'
    DBUSER='caerp@%'
    DBPASS='caerp'
    DBNAME='caerp'
else
    echo "Enter the mysql command line needed to have root access (default: 'mysql -u root')"
    read MYSQLCMD
    if [ "$MYSQLCMD" == '' ]
    then
        MYSQLCMD='mysql -u root'
    fi
    echo "Enter the database name (default : 'caerp')"
    read DBNAME
    if [ "$DBNAME" == '' ]
    then
        DBNAME='caerp'
    fi
    echo "Enter the database user (default : 'caerp@localhost')"
    read DBUSER
    if [ "$DBUSER" == '' ]
    then
        DBUSER='caerp@localhost'
    fi
    echo "Enter the database user password (default : 'caerp')"
    read DBPASS
    if [ "$DBPASS" == '' ]
    then
        DBPASS='caerp'
    fi

fi

if [ "$1" == "docker" ]
then
    echo "Stop caerp container"
    # Le service pserve qui tourne en attendant bloque parfois la connexion
    docker stop caerp
fi
echo "Deleting database ${DBNAME}"
echo "drop database ${DBNAME};" | ${MYSQLCMD}
echo "create database ${DBNAME};" | ${MYSQLCMD}
echo "grant all privileges on ${DBNAME}.* to '${DBUSER}' identified by '${DBPASS}';" | ${MYSQLCMD}
echo "flush privileges;" | ${MYSQLCMD}
echo "Database reseted"
