import { startApp } from '@/helpers/utils'
import App from './App.vue'

const app = startApp(App)
