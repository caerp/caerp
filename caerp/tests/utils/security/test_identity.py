def test_get_identity(
    dbsession, config, get_csrf_request, login, mk_group, mk_access_right
):
    from caerp.utils.security.identity import get_identity

    request = get_csrf_request()
    request.dbsession = dbsession
    avatar = get_identity(request, login.login)
    assert avatar is not None
    assert avatar.lastname == login.user.lastname

    assert get_identity(request, "unknown_user") is None

    group = mk_group(name="group1", label="Groupe 1")
    mk_access_right(name="access_right1", groups=[group])

    login._groups.append(group)
    dbsession.merge(login)

    avatar = get_identity(request, login.login)
    assert avatar is not None
    assert avatar.login._groups[0].name == "group1"
    assert avatar.login._groups[0].access_rights[0].name == "access_right1"
