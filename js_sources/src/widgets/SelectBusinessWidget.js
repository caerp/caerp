import _ from "underscore"
import BaseFormWidget from "./BaseFormWidget.js"
import { getOpt } from "../tools.js"
import Select2RemoteWidget from "./Select2RemoteWidget"

var template = require("./templates/SelectBusinessWidget.mustache")

const SelectBusinessWidget = BaseFormWidget.extend({
    /*
     * A widget to select a customer/project/business
     *
     * Cascade-style selection (customer, then project, then business), it is
     * also possible to select only a customer, or only a project…
     *
     * Use ajax requests.
     */
    tagName: "div",
    className: "form-group",
    template: template,
    regions: {
        select_customer: ".select_customer",
        select_project: ".select_project",
        select_business: ".select_business",
    },
    childViewEvents: {
        finish: "onChildChange",
    },
    childViewTriggers: {
        labelChange: "labelChange",
    },

    renderCustomerSelect(initialValue, initialLabel) {
        this.showChildView(
            "select_customer",
            new Select2RemoteWidget({
                field_name: "customer_id",
                field_id: "customer_id",
                value: initialValue,
                url: this.getOption("customers_url"),
                placeholder: "- Choisir un client -",
                title: "Client",
                value_label: initialLabel,
                id_key: "id",
                label_key: "label",
            })
        )
    },
    renderProjectSelect(initialValue, initialLabel, urlParams) {
        this.showChildView(
            "select_project",
            new Select2RemoteWidget({
                field_name: "project_id",
                field_id: "project_id",
                value: initialValue,
                placeholder: "- Choisir un dossier -",
                url: this.getOption("projects_url"),
                title: "Dossier",
                value_label: initialLabel,
                extraUrlParams: urlParams,
                id_key: "id",
                label_key: "name",
            })
        )
    },
    renderBusinessSelect(initialValue, initialLabel, urlParams) {
        this.showChildView(
            "select_business",
            new Select2RemoteWidget({
                value: initialValue,
                field_name: "business_id",
                field_id: "business_id",
                placeholder: "- Choisir une affaire -",
                url: this.getOption("businesses_url"),
                title: "Affaire",
                value_label: initialLabel,
                extraUrlParams: urlParams,
                id_key: "id",
                label_key: "name",
            })
        )
    },
    onRender: function () {
        let customerId = this.getOption("customer_value")
        let projectId = this.getOption("project_value")
        let businessId = this.getOption("business_value")
        // record for business select
        this.customer_id = customerId

        this.renderCustomerSelect(customerId, this.getOption("customer_label"))
        if (customerId) {
            this.renderProjectSelect(projectId, this.getOption("project_label"))
            if (projectId) {
                this.renderBusinessSelect(
                    businessId,
                    this.getOption("business_label")
                )
            }
        }
    },
    onChildChange(field_name, value) {
        this.triggerMethod("finish", field_name, value)
        if (field_name === "customer_id") {
            // clear business and project on model
            this.triggerMethod("finish", "project_id", null)
            this.triggerMethod("finish", "business_id", null)

            // Re-render project select, filtering by customer
            this.getRegion("select_project").empty()
            if (value) {
                this.renderProjectSelect(null, "", { customer_id: value })
                // Record for business select
                this.customer_id = value
            }

            // Hide business selection (till project is selected)
            this.getRegion("select_business").empty()
        }
        if (field_name === "project_id") {
            // clear business on model
            this.triggerMethod("finish", "business_id", "")

            // Re-render business select, filtering by project_id
            this.getRegion("select_business").empty()
            if (value) {
                this.renderBusinessSelect(null, "", {
                    project_id: value,
                    customer_id: this.customer_id,
                })
            }
        }
    },
    templateContext: function () {
        return this.getCommonContext()
    },
})
export default SelectBusinessWidget
