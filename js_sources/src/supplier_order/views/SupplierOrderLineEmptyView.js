import Mn from "backbone.marionette"

const SupplierOrderLineEmptyView = Mn.View.extend({
    template: require("./templates/SupplierOrderLineEmptyView.mustache"),
    templateContext() {
        var colspan = this.getOption("colspan")
        if (this.getOption("edit")) {
            colspan += 1
        }
        return {
            colspan: colspan,
        }
    },
})
export default SupplierOrderLineEmptyView
