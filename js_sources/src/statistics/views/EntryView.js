import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import { Button } from "bootstrap"

const template = require("./templates/EntryView.mustache")
const EntryView = Mn.View.extend({
    tagName: "tr",
    template: template,
    regions: {},
    ui: {
        edit: "button.edit",
        delete: "button.delete",
        csv_export: "button.csv_export",
        duplicate: "button.duplicate",
    },
    triggers: {
        "click @ui.edit": "model:edit",
        "click @ui.delete": "model:delete",
        "click @ui.csv_export": "model:export",
        "click @ui.duplicate": "model:duplicate",
    },
    childViewEvents: {},
    childViewTriggers: {},
    initialize() {
        this.config = Radio.channel("config")
        this.facade = Radio.channel("facade")
    },
})
export default EntryView
