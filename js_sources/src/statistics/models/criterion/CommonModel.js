import BaseModel from "./BaseModel"

const CommonModel = BaseModel.extend({
    // Define the props that should be set on your model
    props: ["searches", "search1", "search2", "date_search1", "date_search2"],
})
CommonModel.prototype.props = CommonModel.prototype.props.concat(
    BaseModel.prototype.props
)
export default CommonModel
