import Radio from "backbone.radio"

export const isCancelinvoice = function () {
    const config = Radio.channel("config")
    return config.request("get:options", "is_cancelinvoice", false)
}
