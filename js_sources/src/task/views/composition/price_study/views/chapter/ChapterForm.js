import Mn from "backbone.marionette"
import Radio from "backbone.radio"
import ModalFormBehavior from "base/behaviors/ModalFormBehavior"
import InputWidget from "widgets/InputWidget"
import TextAreaWidget from "widgets/TextAreaWidget"
import CheckboxWidget from "widgets/CheckboxWidget"

const template = require("./templates/ChapterForm.mustache")
const ChapterForm = Mn.View.extend({
    partial: false,
    behaviors: [ModalFormBehavior],
    template: template,
    regions: {
        title: ".field-title",
        description: ".field-description",
        display_details: ".field-display_details",
    },
    ui: {},
    events: {},
    childViewEvents: {},
    childViewTriggers: {
        change: "data:modified",
        finish: "data:modified",
        "cancel:click": "cancel:click",
    },
    initialize() {
        this.config = Radio.channel("config")
        this.facade = Radio.channel("facade")
    },
    showTitle() {
        this.showChildView(
            "title",
            new InputWidget({
                value: this.model.get("title"),
                title: "Titre (optionnel)",
                description:
                    "Titre du chapitre tel qu'affiché dans la sortie pdf, laissez vide pour ne pas le faire apparaître",
                field_name: "title",
            })
        )
    },
    showDescription() {
        this.showChildView(
            "description",
            new TextAreaWidget({
                value: this.model.get("description"),
                title: "Description (optionnel)",
                field_name: "description",
                tinymce: true,
                cid: this.model.cid,
            })
        )
    },
    showDisplayDetails() {
        this.showChildView(
            "display_details",
            new CheckboxWidget({
                inline_label:
                    "Afficher le détail des prestations dans le document final",
                description:
                    "En décochant cette case, le chapitre apparaîtra comme une seule ligne de prestation, sans le détail des produits qui le composent.",
                field_name: "display_details",
                value: this.model.get("display_details"),
            })
        )
    },
    onRender() {
        this.showTitle()
        this.showDescription()
        this.showDisplayDetails()
    },
    templateContext() {
        // Collect data sent to the template (model attributes are already transmitted)
        return {
            title: this.getOption("edit")
                ? "Modifier ce chapitre"
                : "Ajouter un chapitre",
        }
    },
    onDestroyModal() {
        let app = Radio.channel("priceStudyApp")
        app.trigger("navigate", "index")
    },
})
export default ChapterForm
