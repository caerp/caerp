import _BaseModel from "base/models/BaseModel"

const BaseModel = _BaseModel.extend({
    props: [
        "id",
        "key",
        "method",
        "type",
        "entry_id",
        "parent_id",
        "relationship",
    ],
})
export default BaseModel
