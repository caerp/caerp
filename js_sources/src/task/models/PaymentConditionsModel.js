import _ from "underscore"
import BaseModel from "../../base/models/BaseModel.js"
import Radio from "backbone.radio"

const PaymentConditionsModel = BaseModel.extend({
    props: ["id", "payment_conditions"],
    validation: {
        payment_conditions: function (value) {
            var channel = Radio.channel("config")
            if (channel.request("has:form_section", "payment_conditions")) {
                if (!value) {
                    return "Veuillez saisir des conditions de paiements"
                }
            }
        },
    },
})
export default PaymentConditionsModel
