/*
 * File Name :  ButtonCollection.js
 */
import Bb from "backbone"
import ButtonModel from "./ButtonModel.js"

const ButtonCollection = Bb.Collection.extend({
    model: ButtonModel,
})
export default ButtonCollection
