/*
 * Module name : DiscountEmptyView
 */
import Mn from "backbone.marionette"
import Radio from "backbone.radio"

const template = require("./templates/DiscountEmptyView.mustache")

const DiscountEmptyView = Mn.View.extend({
    template: template,
})
export default DiscountEmptyView
