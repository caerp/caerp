/*
 * File Name :  CategoryCollection
 */
import Bb from "backbone"
import CategoryModel from "./CategoryModel.js"
import { sortCollection } from "../../tools"

const CategoryCollection = Bb.Collection.extend({
    model: CategoryModel,
    initialize() {
        sortCollection(this, "title", "asc")
    },
})
export default CategoryCollection
