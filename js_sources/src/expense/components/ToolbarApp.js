import BaseToolbarAppClass from "../../common/components/ToolbarAppClass"
import ActionModel from "../../common/models/ActionModel"
import ToggleButtonWidget from "../../widgets/ToggleButtonWidget"
import Radio from "backbone.radio"

function notifyJustifiedChanged(model) {
    let newValue = model.get("options").current_value
    Radio.channel("facade").trigger("status:justified:changed", newValue)
}

const ToolbarAppClass = BaseToolbarAppClass.extend({
    channelName: "facade",
    radioRequests: {
        "set:globalJustified": "setGlobalJustified",
    },
    setGlobalJustified(value) {
        if (this.resume_view) {
            this.resume_view.model.get("options").current_value = value
            this.resume_view.updateSelectOptions()
            this.resume_view.render()
        }
    },
    getResumeView(actions) {
        let resume_view = null
        if (!_.isUndefined(actions["justify"])) {
            const model = new ActionModel(actions["justify"])
            resume_view = new ToggleButtonWidget({ model: model })
            resume_view.on("status:changed", notifyJustifiedChanged)
            this.resume_view = resume_view
        }
        return resume_view
    },
})
const ToolbarApp = new ToolbarAppClass()
export default ToolbarApp
